#!/usr/bin/env python

import unittest
import worldstove.spritegen as spritegen
import numpy as np
from PIL import Image

class TestRandomTree(unittest.TestCase):
	preview = False

	def test_simple_treegen(self):
		cls = spritegen.SimpleTree
		for i in range(3): 
			img = cls().generate()
			if self.preview: spritegen.preview(img)
			#Image.fromarray(gen()).save('test_out/tree_{:02d}.png'.format(i))

	def test_oval_treegen(self):
		cls = spritegen.OvalTree
		for i in range(3): 
			img = cls().generate()
			if self.preview: spritegen.preview(img)
			#Image.fromarray(gen()).save('test_out/tree_{:02d}.png'.format(i))

	def test_concave_oval_treegen(self):
		cls = spritegen.OvalTree
		for i in range(3): 
			img = cls(concavity=2).generate()
			if self.preview: spritegen.preview(img)
			#Image.fromarray(gen()).save('test_out/tree_{:02d}.png'.format(i))
	
	def dont_test_winter_treegen(self):
		cls = spritegen.OvalTree
		for i in range(3): 
			tree = cls()
			tree.generate()
			if self.preview: spritegen.preview(tree.image)
			#Image.fromarray(tree.image).save('test_out/tree_{:02d}_1.png'.format(i))
			tree.winterize()
			#Image.fromarray(tree.image).save('test_out/tree_{:02d}_2.png'.format(i))
			#if self.preview: spritegen.preview(tree.image)

	def test_evergreen_treegen(self):
		cls = spritegen.SimpleEvergreenTree
		for i in range(3): 
			tree = cls()
			tree.generate()
			if self.preview: spritegen.preview(tree.image)
			Image.fromarray(tree.image).save('test_out/tree_{:02d}_1.png'.format(i))
			tree.winterize()
			Image.fromarray(tree.image).save('test_out/tree_{:02d}_2.png'.format(i))
			if self.preview: spritegen.preview(tree.image)

class TestRandomGrass(unittest.TestCase):
	preview = False
	def test_simple_grassgen(self):
		cls = spritegen.SimpleGrass
		for i in range(3):
			grass = cls()
			grass.generate()

			if self.preview: spritegen.preview(grass.image)

class TestRandomMountain(unittest.TestCase):
	preview = True
	def test_simple_mountaingen(self):
		cls = spritegen.SimpleMountainChain
		points1 = np.array([[100,100]])
		mountains = cls()
		mountains.generate()

		if self.preview: spritegen.preview(mountains.image)

	def test_simple_hillgen(self):
		cls = spritegen.SimpleHillChain
		points1 = np.array([[100,100]])
		mountains = cls()
		mountains.generate()

		if self.preview: spritegen.preview(mountains.image)
if __name__ == '__main__': unittest.main()
