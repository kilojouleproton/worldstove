#!/usr/bin/env python

import unittest
import worldstove.noise as noise
import matplotlib.pyplot as plt
import numpy as np

class TestPoissonDisk(unittest.TestCase):
	def test_arghandling(self):
		with self.assertRaises(TypeError):
			pds = noise.PoissonDiskSampler(radius=1, count=100)

		with self.assertRaises(TypeError):
			pds = noise.PoissonDiskSampler(radius=1, radiusmap=np.eye(3))

		with self.assertRaises(TypeError):
			pds = noise.PoissonDiskSampler(count=100, radiusmap=np.eye(3))

		with self.assertRaises(TypeError):
			pds = noise.PoissonDiskSampler(densitymap=np.eye(3), radiusmap=np.eye(3))

		with self.assertRaises(TypeError):
			pds = noise.PoissonDiskSampler()

	def test_uniform(self):
		shape = (200, 100)
		pds = noise.PoissonDiskSampler(shape, radius=20)
		pds.generate()

		assert len(pds.points) < np.prod(pds.grid.shape)


	def test_uniform_preplaced(self):
		shape = (200, 100)
		points = np.array([[100, 50]])
		pds = noise.PoissonDiskSampler(shape, points=points, radius=20)
		pds.generate()

		shape = (200, 100)
		points = np.array([[10, 5], [100, 50]])
		pds = noise.PoissonDiskSampler(shape, points=points, radius=20)
		pds.generate()

		shape = (200, 100)
		points = np.array([[10, 5], [20, 5]])
		pds = noise.PoissonDiskSampler(shape, points=points, radius=20)
		with self.assertRaises(IndexError): pds.generate()

	def test_scaled(self):
		densitymap = np.vstack([np.arange(1, 10, 0.1) ** 1] * 400).T

		#densitymap[200:700,50:350] = 0.3

		radius = 200
		pds = noise.PoissonDiskSampler(shape=densitymap.shape, radius=radius, densitymap=densitymap)
		pds.generate()
		preview(pds)

def preview(pds):
	if pds.densitymap is not None: plt.imshow(pds.densitymap.T, cmap='inferno')
	plt.plot(pds.points[:,0], pds.points[:,1], marker='.', lw=0)
	plt.xlim([0, pds.shape[0]])
	plt.ylim([0, pds.shape[1]])
	plt.xticks(np.arange(0, pds.shape[0] + pds.gridspacing, pds.gridspacing))
	plt.yticks(np.arange(0, pds.shape[1] + pds.gridspacing, pds.gridspacing))
	plt.grid('on')
	plt.show()

if __name__ == '__main__': unittest.main()
