import xml.etree.ElementTree as ET
import os
import warnings
import re

TERRAINCODES = {
	'PLAINS': 0,
	'SMALLPROV': 2**0,
	'LARGEPROV': 2**1,
	'SEA': 2**2,
	'FRESHWATER': 2**3, 
	'HIGHLAND': 2**4,
	'SWAMP': 2**5,
	'WASTE': 2**6,
	'FOREST': 2**7,
	'FARM': 2**8,
	'NOSTART': 2**9, 
	'MANYSITES': 2**10, 
	'DEEPSEA': 2**11,
	'CAVE': 2**12,
	'MOUNTAINS': 2**22,
	'THRONE': 2**24,
	'START': 2**25,
	'NOTHRONE': 2**26,
	'WARMER': 2**29,
	'COLDER': 2**30,

	#not observed, extrapolating codes
	'FIRESITES': 2**13,
	'AIRSITES': 2**14,
	'WATERSITES': 2**15,
	'EARTHSITES': 2**16,
	'ASTRALSITES': 2**17,
	'DEATHSITES': 2**18,
	'NATURESITES': 2**19,
	'BLOODSITES': 2**20,
	'HOLYSITES': 2**21,
}

class Nation(object):
	center = None
	ring = None
	def __init__(self, name='untitled', id=-1, waterpercentage=0, age=None, capringsize=5, capterrain=None, capring=None):
		self.name = 'untitled' 
		self.id = id
		self.waterpercentage = waterpercentage
		self.age = age
		self.capringsize = capringsize
		self.capterrain = capterrain
		self.capring = [] if capring is None else capring

	def __repr__(self):
		return 'Nation("{}")'.format(self.name)

	@staticmethod
	def from_xml(tag):
		nation = Nation()
		for prop in tag:
			if prop.tag == 'Name': nation.name = prop.text
			elif prop.tag == 'ID': nation.id = int(prop.text)
			elif prop.tag == 'WaterPercentage': nation.waterpercentage = float(prop.text)
			elif prop.tag == 'Age': nation.age = prop.text
			elif prop.tag == 'CapRingSize': nation.capringsize = int(prop.text)
			elif prop.tag == 'CapTerrain': nation.capterrain = prop.text.split()
			elif prop.tag == 'TerrainData': nation.capring.append(prop.text.split())
			else: warnings.warn('Unrecognized tag {}: Worldstove may be out of date'.format(prop.tag))

		return nation

class NukeParser(object):
	def parse(fh):
		doc = ET.parse(fh)
		root = doc.getroot()
		nations = []
		for tag in root: nations.append(Nation.from_xml(tag))
		return nations


def load_all(nationdir):
	nations = []
	for basename in os.listdir(nationdir):
		if basename.endswith('.xml'):
			with open('{}/{}'.format(nationdir, basename)) as fh:
				nations.extend(NukeParser.parse(fh))
	return nations

def _simplify(text):
	return re.sub('[^a-z]', '', text.lower())

def find_nation(pattern, nationlist):
	simplified = _simplify(pattern)
	for nation in nationlist:
		if simplified == _simplify(nation.name): return nation
	for nation in nationlist:
		if _simplify(nation.name).startswith(simplified): return nation
	raise KeyError('Could not find "{}" in the given nationlist'.format(pattern))
