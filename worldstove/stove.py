import numpy as np
import warnings
from . import nukeparser 
from scipy.spatial import Voronoi

ANSI_RESET = '\033[0m'

def extract_bit(num, pos): return (num >> pos) % 2

def cfg_roll(config, section, prefix):
	bounds = config.getint(section, prefix + 'min'), config.getint(section, prefix + 'max') 
	return np.random.uniform(bounds[0], bounds[1])/100

def introll(num):
	if num % 1: return int(np.random.uniform(np.floor(num), np.ceil(num)))
	else: return int(num)

class Province(object):

	gridpos = None
	imagepos = None
	neighbors = None

	locked = False

	startsite = False
	thronesite = False

	terrain = 0

	def __init__(self, gridpos, imagepos=None, terrain=0, connections=None):
		self.gridpos = gridpos
		self.imagepos = imagepos
		self.terrain = terrain

		self.connections = [] if connections is None else connections

	def next_to(self, other):
		for conn in self.connections:
			if other in conn: return True
		return False

	def connection_to(self, other):
		for conn in self.connections:
			if other in conn: return conn
		raise IndexError('No connection between {} and {}'.format(self, other))

	def is_plain(self): return self.terrain == 0
	def is_water(self): return extract_bit(self.terrain, 2)
	def is_freshwater(self): return extract_bit(self.terrain, 3) & extract_bit(self.terrain, 2)
	def is_lake(self): return extract_bit(self.terrain, 3) & extract_bit(self.terrain, 2)
	def is_highland(self): return extract_bit(self.terrain, 4)
	def is_swamp(self): return extract_bit(self.terrain, 5)
	def is_waste(self): return extract_bit(self.terrain, 6)
	def is_forest(self): return extract_bit(self.terrain, 7)
	def is_farm(self): return extract_bit(self.terrain, 8)
	def is_deep(self): return extract_bit(self.terrain, 11)
	def is_cave(self): return extract_bit(self.terrain, 12)
	def is_mountain(self): return extract_bit(self.terrain, 22)

	def get_neighbors(self):
		neighbors = []
		for conn in self.connections:
			if conn[0] is self: neighbors.append(conn[1])
			else: neighbors.append(conn[0])
		return neighbors

	def get_ansi(self):
		if self.is_water():  #sea
			if extract_bit(self.terrain, 11): #deep sea
				#return '\033[44m'
				return '\033[48;5;19m'
			else: #shallow seas, lakes
				#return '\033[46;30m'
				return '\033[30;48;5;27m'
		elif extract_bit(self.terrain, 12): #cave
			#return '\033[45m'
			return '\033[48;5;53m'
		elif extract_bit(self.terrain, 7): #forest		
			#return '\033[42m'
			return '\033[48;5;22m'
		elif extract_bit(self.terrain, 4): #highlands
			#return '\033[47;30m'
			return '\033[48;5;58m'
		elif extract_bit(self.terrain, 22): #mountains
			return '\033[48;5;101m'
		elif extract_bit(self.terrain, 6): #waste
			#return '\033[41;30m'
			return '\033[48;5;216m'
		elif extract_bit(self.terrain, 8): #farm
			return '\033[30;48;5;214m'

		#else: return '\033[48;5;223m'
		else: return '\033[30;48;5;107m'

	def __hash__(self): return hash((type(self), self.gridpos))

	def __add__(self, other): return [self.gridpos[i] + other.gridpos[i] for i in range(2)]
	def __sub__(self, other): return [self.gridpos[i] - other.gridpos[i] for i in range(2)]
	def __repr__(self): return 'Province({}, {})'.format(*self.gridpos)

	def __getitem__(self, index): return self.gridpos[index]

class Connection(object):
	provinces = None
	special = 0

	def __init__(self, prov1, prov2):
		self.provinces = [prov1, prov2]

	def get_mutual_neighbors(self):
		return list(set(self[0].get_neighbors()).intersection(set(self[1].get_neighbors())))

	def get_ansi(self):
		if self.special == 1: return '\033[38;5;5m' #mountain pass
		elif self.special == -2: return '\033[38;5;75m' #bridged river
		elif self.special == 2: return '\033[38;5;21m' #river
		elif self.special == 4: return '\033[38;5;89m' #unwalkable
		elif self.special == 8: return '\033[38;5;130m' #road
		else: return ''

	def __iter__(self): return iter(self.provinces)
	def __getitem__(self, index): return self.provinces[index]
	def __setitem__(self, newval): self.provinces[index] = newval
	def __hash__(self): return hash((type(self), self.provinces[0], self.provinces[1]))

	def __repr__(self): return 'Connection({}, {})'.format(*self.provinces)

	def get_disp(self): return self.provinces[1] - self.provinces[0]

class Grid(object):
	shape = None
	imshape = None
	provinces = None
	connections = None
	voronoi = None
	capring = 5
	watersize = 3

	def __init__(self, shape, imshape):
		self.shape = shape
		self.imshape = imshape
		self.provinces = []
		self.connections = []
		
		for r in range(shape[0]):
			self.provinces.append([])
			for c in range(shape[1]):
				x = ((c + 0.5 + np.clip(np.random.normal(0, 0.15), -0.4, 0.4)) / self.shape[1] * self.imshape[1]) % self.imshape[1]
				y = ((r + 0.5 + np.clip(np.random.normal(0, 0.15), -0.4, 0.4)) / self.shape[0] * self.imshape[0]) % self.imshape[0]

				self.provinces[-1].append(Province((r,c), (x,y)))
		for r in range(shape[0]):
			for c in range(shape[1]):
				hnext = self.provinces[r][(c+1) % shape[1]]
				vnext = self.provinces[(r+1) % shape[0]][c]
				hconn = Connection(self.provinces[r][c], hnext)
				vconn = Connection(self.provinces[r][c], vnext)

				self.provinces[r][c].connections.append(hconn)
				hnext.connections.append(hconn)

				self.provinces[r][c].connections.append(vconn)
				vnext.connections.append(vconn)

				self.connections.extend([hconn, vconn])

	def add_random_connections(self):
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				if np.random.randint(2):
					prov1 = self.provinces[r][c]
					prov2 = self.provinces[(r+1) % self.shape[0]][(c+1) % self.shape[1]]
				else:
					prov1 = self.provinces[(r+1) % self.shape[0]][c]
					prov2 = self.provinces[r][(c+1) % self.shape[1]]

				conn = Connection(prov1, prov2)
				prov1.connections.append(conn)
				prov2.connections.append(conn)
				self.connections.append(conn)

		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				here = self.provinces[r][c]
				r2 = (r+1) % self.shape[0]
				c2 = (c+1) % self.shape[1]

				if not here.startsite: continue
				if len(here.connections) > self.capring:
					choices = np.arange(4, len(here.connections))
					np.random.shuffle(choices)
					choices = sorted(choices[:4-self.capring])[::-1]

					for i in choices:
						conn = here.connections[i]
						self.flip_diagonal(conn)


				elif len(here.connections) < self.capring:
					choices = []
					for dr in -1, 1:
						for dc in -1, 1:
							r2 = (r+dr) % self.shape[0]
							c2 = (c+dc) % self.shape[1]
							if here.next_to(self.provinces[r2][c2]): continue
							else: choices.append((dr,dc))
					np.random.shuffle(choices)
					while len(here.connections) < self.capring:
						dr, dc = choices.pop()

						r2 = (r+dr) % self.shape[0]
						c2 = (c+dc) % self.shape[1]

						oldprov1 = self.provinces[r][c2]
						oldprov2 = self.provinces[r2][c]
						self.flip_diagonal(self.get_connection(oldprov1, oldprov2))
						

	def get_connection(self, prov1, prov2):
		#TODO: THIS IS HORRIBLY INEFFICIENT
		#maybe track connections on 0.5 indices or something
		for conn1 in prov1.connections:
			for conn2 in prov2.connections:
				if conn1 == conn2: return conn1
		raise IndexError('No connection between {} and {}')

	def flip_diagonal(self, connection):
		oldprov1, oldprov2 = connection.provinces

		r = [oldprov1[0], oldprov2[0]]
		c = [oldprov1[1], oldprov2[1]]

		newprov1 = self.provinces[r[0]][c[1]]
		newprov2 = self.provinces[r[1]][c[0]]

		oldprov1.connections.remove(connection)
		oldprov2.connections.remove(connection)
		connection.provinces = [newprov1, newprov2]
		newprov1.connections.append(connection)
		newprov2.connections.append(connection)

	def add_starts_thrones(self, players, thrones):
		crowding = np.zeros(self.shape, dtype=np.int64)
		playerprov = self.shape[0] * self.shape[1] / players
		radius = max(3, np.int64(np.ceil(np.sqrt(playerprov)/2)))

		for i in range(players):
			placed = False
			for v in range(int(np.max(crowding[crowding < 999])) + 1):
				choices = np.vstack(np.nonzero(crowding == v)).T
				try: p = choices[np.random.randint(choices.shape[0]),:]
				except ValueError: continue

				self[p[0]][p[1]].startsite = True

				for dr in range(-radius, radius+1):
					for dc in range(-radius, radius+1):
						if dr == 0 and dc == 0: score = 999
						else: score = radius - np.max([np.abs(dr), np.abs(dc)])
						placed = True

						r2 = (p[0]+dr) % self.shape[0]
						c2 = (p[1]+dc) % self.shape[1]
						#crowding[r2,c2] += score
						crowding[r2,c2] = max(score, crowding[r2,c2])
				if placed: break

		for i in range(thrones):
			placed = False
			for v in range(int(np.max(crowding[crowding < 999])) + 1):
				choices = np.vstack(np.nonzero(crowding == v)).T
				try: p = choices[np.random.randint(choices.shape[0]),:]
				except ValueError: continue

				self[p[0]][p[1]].thronesite = True

				for dr in range(-radius, radius+1):
					for dc in range(-radius, radius+1):
						if dr == 0 and dc == 0: score = 999
						else: score = radius - np.max([np.abs(dr), np.abs(dc)])
						placed = True

						r2 = (p[0]+dr) % self.shape[0]
						c2 = (p[1]+dc) % self.shape[1]
						#crowding[r2,c2] += score
						crowding[r2,c2] = max(score, crowding[r2,c2])
				if placed: break

	def apply_starts(self, nations, clustered_water=True):
		print('nations', ', '.join([nation.name for nation in nations]))
		nationscopy = nations[:]
		np.random.shuffle(nationscopy)

		waternations = 0
		for nation in nationscopy:
			if nation.waterpercentage: 
				waternations += 1

		starts = []
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				province = self[r][c]
				if province.startsite: starts.append(province)

		if clustered_water and waternations >= 2:
			distances = 999 * np.ones((len(starts), len(starts)), dtype=np.int64)
			for i, s1 in enumerate(starts):
				for j, s2 in enumerate(starts):
					if j <= i: continue

					vdist = min(abs(s2.gridpos[0] - s1.gridpos[0]), abs(s2.gridpos[0] - s1.gridpos[0] + self.shape[0]))
					hdist = min(abs(s2.gridpos[1] - s1.gridpos[1]), abs(s2.gridpos[1] - s1.gridpos[1] + self.shape[1]))
					#just use manhattan distance for now
					distances[i,j] = vdist + hdist
					distances[j,i] = vdist + hdist

			waterstarts = []
			choices = np.vstack(np.nonzero(distances == np.min(distances))).T
			np.random.shuffle(choices)
			waterstarts.extend(choices[0,:])
			while len(waterstarts) < waternations:
				submatrix = distances[np.int64(waterstarts),:]
				mindists = 999 * np.ones(len(starts), dtype=np.int64)
				for i, s1 in enumerate(starts):
					if i in waterstarts: continue
					mindists[i] = np.min(submatrix[:,i])

				choices = np.nonzero(mindists == np.min(mindists))[0]
				np.random.shuffle(choices)
				waterstarts.append(choices[0])

			land = []
			water = []
			for nation in nations:
				if nation.waterpercentage: water.append(nation)
				else: land.append(nation)
			np.random.shuffle(water)
			np.random.shuffle(land)

			startcount = 0
			for r in range(self.shape[0]):
				for c in range(self.shape[1]):
					province = self[r][c]
					if province.startsite: startcount += 1

			startid = 0
			for r in range(self.shape[0]):
				for c in range(self.shape[1]):
					province = self[r][c]
					if province.startsite:
						if startid in waterstarts: nation = water.pop()
						else: nation = land.pop()
						startid += 1

						province.specstart = nation.id

						for code in nation.capterrain:
							province.terrain |= nukeparser.TERRAINCODES[code]
						province.locked = True

						neighbors = province.get_neighbors()
						np.random.shuffle(neighbors)
						for neighbor, codelist in zip(neighbors, nation.capring):
							for code in codelist:
								neighbor.terrain |= nukeparser.TERRAINCODES[code]
								neighbor.locked = True


		else:
			
			for r in range(self.shape[0]):
				for c in range(self.shape[1]):
					province = self[r][c]
					if province.startsite:
						nation = nationscopy.pop()
						province.specstart = nation.id

						for code in nation.capterrain:
							province.terrain |= nukeparser.TERRAINCODES[code]
						province.locked = True

						neighbors = province.get_neighbors()
						np.random.shuffle(neighbors)
						for neighbor, codelist in zip(neighbors, nation.capring):
							for code in codelist:
								neighbor.terrain |= nukeparser.TERRAINCODES[code]
								neighbor.locked = True

	def get_all_provinces(self):
		out = []
		for row in self.provinces: out.extend(row)
		return out

	def get_water(self, deep=None):
		out = []
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				province = self[r][c]
				if deep is None:
					if province.is_water(): out.append(province)
				else:
					if province.is_water() and (deep == extract_bit(province.terrain, 11)): out.append(province)
		return out

	def generate_terrain(self, nations, config):
		#TODO: Tie CLI args to overriding the default config
		targetwater = np.mean([nation.waterpercentage for nation in nations]) * self.shape[0] * self.shape[1]

		currentwater = set()
		if targetwater:
			currentwater = set(self.get_water())
			timeout = 5
			while (len(currentwater) < targetwater) and (timeout > 0):
				currentwater = set(self.get_water())
				coasts = []
				for province in currentwater:
					for neighbor in province.get_neighbors():
						if neighbor not in currentwater: 
							coasts.append(neighbor)
							#put a slight bias on surrounding deep provinces with shallow seas
							if extract_bit(province.terrain, 11): coasts.append(neighbor)
				np.random.shuffle(coasts)
				grown = False
				for province in coasts:
					if province.locked: continue
					else: 
						grown = True
						province.terrain |= 2**2
						break
				if grown: continue
				else: 
					for province in self.get_all_provinces(): 
						if province.locked: continue
						elif province.terrain == 0: 
							province.terrain |= 2**2
							break
					timeout -= 1
			if timeout == 0: warnings.warn('Could not expand seas adequately, skipping at {} out of a target of {}'.format(len(currentwater), targetwater))

		area =  self.shape[0] * self.shape[1]
		if config.getint('terrains', 'lakemax', fallback=7) > 0:
			currentwater.update(self.add_lakes(cfg_roll(config, 'terrains', 'lake') * (area - len(currentwater))))

		self.assign_sizes(cfg_roll(config, 'sizes', 'large') * area, cfg_roll(config, 'sizes', 'small') * area)
		#def assign_terrains(self, targetswamp, targetwaste, targetmountain, targethighland, targetcave, targetforest, targetfarm):
		swamp = cfg_roll(config, 'terrains', 'swamp') * area
		waste = cfg_roll(config, 'terrains', 'wasteland') * area
		mountain = cfg_roll(config, 'terrains', 'mountain') * area
		highland = cfg_roll(config, 'terrains', 'highland') * area
		cave = cfg_roll(config, 'terrains', 'cave') * area
		forest = cfg_roll(config, 'terrains', 'forest') * area
		farm = cfg_roll(config, 'terrains', 'farm') * area
		self.assign_terrains(swamp, waste, mountain, highland, cave, forest, farm)

		#NOTE: assign_water_terrain treats deeps as mountains, so we'll 
		#preserve that behavior here

		print('sea', len(currentwater), '/', round(targetwater))

	def assign_sizes(self, targetlarge, targetsmall):
		unlocked = np.ones(self.shape, dtype=np.int64)
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				if self[r][c].locked: unlocked[r,c] = 0

		choices = list(np.vstack(np.nonzero(unlocked)).T)
		currentlarge = 0
		currentsmall = 0
		for i in range(0, int(round(targetlarge))):
			r, c = choices.pop()
			self[r][c].terrain |= 2**1
			currentlarge += 1
		for i in range(0, int(round(targetsmall))):
			r, c = choices.pop()
			self[r][c].terrain |= 2**0
			currentsmall += 1

		print('large:', currentlarge, '/', round(targetlarge))
		print('small:', currentsmall, '/', round(targetsmall))

	def assign_terrains(self, targetswamp, targetwaste, targetmountain, targethighland, targetcave, targetforest, targetfarm):
		unlocked = np.ones(self.shape, dtype=np.bool8)
		land = np.ones(self.shape, dtype=np.bool8)
		water = np.zeros_like(land)
		therecanbeonlyone = np.zeros(self.shape, dtype=np.bool8)

		currentswamp = currentwaste = currentmountain = currenthighland = currentcave = currentforest = currentfarm = 0
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				if self[r][c].locked: unlocked[r,c] = False
				if self[r][c].is_water(): 
					land[r,c] = False
					water[r,c] = True

				if extract_bit(self[r][c].terrain, 4): currenthighland += 1
				if extract_bit(self[r][c].terrain, 5): currentswamp += 1
				if extract_bit(self[r][c].terrain, 6): currentwaste += 1
				if extract_bit(self[r][c].terrain, 7): currentforest += 1
				if extract_bit(self[r][c].terrain, 8): currentfarm += 1
				if extract_bit(self[r][c].terrain, 11): currentmountain += 1
				if extract_bit(self[r][c].terrain, 22): currentmountain += 1
				if extract_bit(self[r][c].terrain, 12): currentcave += 1


		#THINGS WHAT GO IN WATER ONLY
		choices = list(np.vstack(np.nonzero(unlocked & ~land & ~therecanbeonlyone)).T)
		np.random.shuffle(choices)
		for i in range(currentmountain, int(round(targetmountain * np.sum(water) / np.prod(water.shape)))):
			r, c = choices.pop()
			currentmountain += 1
			therecanbeonlyone[r,c] = True
			if land[r,c]: self[r][c].terrain |= 2**22
			else: self[r][c].terrain |= 2**11

		#THINGS WHAT GO ON LAND OR WATER
		choices = list(np.vstack(np.nonzero(unlocked & ~therecanbeonlyone)).T)
		np.random.shuffle(choices)
		#FORESTS
		#choices = list(np.vstack(np.nonzero(unlocked & ~therecanbeonlyone)).T)
		#np.random.shuffle(choices)
		for i in range(currentforest, int(round(targetforest))):
			r, c = choices.pop()
			currentforest += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**7

		#CAVES
		#choices = list(np.vstack(np.nonzero(unlocked & ~therecanbeonlyone)).T)
		#np.random.shuffle(choices)
		for i in range(currentcave, int(round(targetcave))):
			r, c = choices.pop()
			currentcave += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**12
			if np.random.random() < 0.2: self[r][c].terrain |= 2**7 #the elusive cave forest!

		#HIGHLANDS/GORGES
		for i in range(currenthighland, int(round(targethighland))):
			r, c = choices.pop()
			currenthighland += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**4

		#THINGS WHAT GO ON LAND ONLY
		#FARMS
		choices = list(np.vstack(np.nonzero(unlocked & land & ~therecanbeonlyone)).T)
		np.random.shuffle(choices)

		for i in range(currentwaste, int(round(targetwaste))):
			try: r, c = choices.pop()
			except IndexError: break
			currentwaste += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**6

		for i in range(currentswamp, int(round(targetswamp))):
			try: r, c = choices.pop()
			except IndexError: break
			currentswamp += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**5

		for i in range(currentfarm, int(round(targetfarm))): #TODO: bias farms to more distant provinces
			try: r, c = choices.pop()
			except IndexError: break
			currentfarm += 1
			therecanbeonlyone[r,c] = True
			self[r][c].terrain |= 2**8

		print('highland:', currenthighland, '/', round(targethighland))
		print('swamp:', currentswamp, '/', round(targetswamp))
		print('waste:', currentwaste, '/', round(targetwaste))
		print('forest:', currentforest, '/', round(targetforest))
		print('farm:', currentfarm, '/', round(targetfarm))
		print('cave:', currentcave, '/', round(targetcave))
		print('mountain:', currentmountain, '/', round(targetmountain))

	def assign_connections(self, config):

		landland = []
		for conn in self.connections:
			if conn[0].locked or conn[1].locked: continue
			if not (conn[0].is_water() or conn[1].is_water()): 
				landland.append(conn)

		targetroad = introll(cfg_roll(config, 'connections', 'road') * len(landland))
		targetmountainpass = introll(cfg_roll(config, 'connections', 'mountainpass') * len(landland))
		targetriver = introll(cfg_roll(config, 'connections', 'river') * len(landland))
		targetbridge = introll(cfg_roll(config, 'connections', 'bridge') * len(landland))
		targetmountain = introll(cfg_roll(config, 'connections', 'mountain') * len(landland))

		currentroad = currentmountainpass = currentriver = currentbridge = currentmountain = 0

		print(targetroad, targetmountainpass, targetriver, targetbridge, targetmountain)

		np.random.shuffle(landland)

		#PLACE OBSTRUCTIONS
		timeout = 30
		while ((currentriver < targetriver) or (currentbridge < targetbridge) or (currentmountain < targetmountain) or (currentmountainpass < targetmountainpass)) and (timeout > 0):
			try: conn = landland.pop()
			except IndexError: warnings.warn('Could not find any land-land connections, skipping obstruction placement')

			p1, p2 = conn.get_mutual_neighbors()
			didsomething = False
			if p1.is_water() or p2.is_water():
				if (currentriver < targetriver) or (currentbridge < targetbridge): 
					riverleft = targetriver - currentriver
					bridgeleft = targetbridge - currentbridge
					if np.random.uniform(riverleft + bridgeleft) < riverleft: 
						conn.special = 2
						currentriver += 1
					else:	
						conn.special = -2
						currentbridge += 1
					didsomething = True
			else:
				if (currentmountain < targetmountain) or (currentmountainpass < targetmountainpass):
					mountainleft = targetmountain - currentmountain
					passleft = targetmountainpass - currentmountainpass
					if np.random.uniform(mountainleft + passleft) < mountainleft: 
						conn.special = 4
						currentmountain += 1
					else:
						conn.special = 1
						currentmountainpass += 1
					didsomething = True
			if not didsomething: timeout -= 1

		landland = []
		for conn in self.connections:
			if conn.special: continue
			if conn[0].locked or conn[1].locked: continue
			if not (conn[0].is_water() or conn[1].is_water()): 
				landland.append(conn)
		np.random.shuffle(landland)

		#PLACE ROADS
		for i in range(currentroad, int(round(targetroad))):
			conn = landland.pop()
			conn.special = 8
			currentroad += 1

		print('road:', currentroad, '/', round(targetroad))
		print('bridge:', currentbridge, '/', round(targetbridge))
		print('river:', currentriver, '/', round(targetriver))
		print('mountain:', currentmountain, '/', round(targetmountain))
		print('pass:', currentmountainpass, '/', round(targetmountainpass))

	def add_lakes(self, targetlakes):
		currentlakes = 0
		seen = set()
		timeout = 10
		out = []
		while (currentlakes < targetlakes) and (timeout > 0):

			nearwater = np.zeros(self.shape, dtype=np.int64)
			salt = np.zeros(self.shape, dtype=np.int64)
			for r in range(self.shape[0]):
				for c in range(self.shape[1]):
					if self[r][c].is_water(): 
						nearwater[r,c] = 1
						if not self[r][c].is_freshwater(): salt[r,c] = 1
			nearwater += np.roll(nearwater, 1, axis=0)
			nearwater += np.roll(nearwater, -1, axis=0)
			nearwater += np.roll(nearwater, 1, axis=1)
			nearwater += np.roll(nearwater, -1, axis=1)

			nearwater += np.roll(np.roll(nearwater, 1, axis=0), 1, axis=1)
			nearwater += np.roll(np.roll(nearwater, 1, axis=0), -1, axis=1)
			nearwater += np.roll(np.roll(nearwater, -1, axis=0), 1, axis=1)
			nearwater += np.roll(np.roll(nearwater, -1, axis=0), -1, axis=1)

			nearsalt = np.zeros_like(salt)
			nearsalt += np.roll(salt, 1, axis=0)
			nearsalt += np.roll(salt, -1, axis=0)
			nearsalt += np.roll(salt, 1, axis=1)
			nearsalt += np.roll(salt, -1, axis=1)

			nearsalt += np.roll(np.roll(salt, 1, axis=0), 1, axis=1)
			nearsalt += np.roll(np.roll(salt, 1, axis=0), -1, axis=1)
			nearsalt += np.roll(np.roll(salt, -1, axis=0), 1, axis=1)
			nearsalt += np.roll(np.roll(salt, -1, axis=0), -1, axis=1)

			#choices = np.vstack(np.nonzero((nearwater == np.min(nearwater)) & ~nearsalt)).T
			choices = np.vstack(np.nonzero(nearsalt == 0)).T
			if not choices.shape[0]: 
				timeout -= 1
				continue
			np.random.shuffle(choices)
			added = False
			for r, c in choices:
				if self[r][c].locked: continue
				if (r, c) in seen: continue
				self[r][c].terrain |= 2**2
				#TODO: check for saltwater neighbors
				#no, wait, we already did that, silly
				self[r][c].terrain |= 2**3
				seen.add((r, c))
				out.append(self[r][c])
				currentlakes += 1
				added = True
				break
			if not added: timeout -= 1
		print('lakes', currentlakes, '/', round(targetlakes))
		return out
			

	def get_land(self):
		out = []
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				if self[r][c].is_water(): pass
				else: out.append(self[r][c])
		return out

	def get_water(self):
		out = []
		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				if self[r][c].is_water(): out.append(self[r][c])
		return out

	def get_nearby(self, province):
		if isinstance(province, tuple): r0, c0 = province
		elif isinstance(province, int): r0, c0 = self.get_all_provinces()[province].gridpos
		else: r0, c0 = province.gridpos

		provinces = []
		for dr in -1, 0, 1:
			for dc in -1, 0, 1:
				if not (dr or dc): continue
				provinces.append(self.provinces[(r0+dr) % self.shape[0]][(c0+dc) % self.shape[1]])
		return provinces

	def pretty_print(self, color=False):
		out = ''
		for r in range(self.shape[0]):
			row = ''
			for c in range(self.shape[1]):
				here = self.provinces[r][c]
				if color: row += here.get_ansi()

				if here.startsite: row += '@'
				elif here.thronesite: row += '╥'
				else: row += '*'

				if color: row += ANSI_RESET

				hnext = self.provinces[r][(c+1) % self.shape[1]]

				if here.next_to(hnext): 
					if color: row += here.connection_to(hnext).get_ansi()
					row += '-'
					if color: row += ANSI_RESET
				else: row += ' '
			row += '\n'
			out += row
			row = ''
			for c in range(self.shape[1]):
				here = self.provinces[r][c]
				vnext = self.provinces[(r-1) % self.shape[0]][c]

				if here.next_to(vnext): 
					if color: row += here.connection_to(vnext).get_ansi()
					row += '|'
					if color: row += ANSI_RESET
				else: row += ' '

				r2 = (r+1) % self.shape[0]
				c2 = (c+1) % self.shape[1]
				if here.next_to(self.provinces[r2][c2]) and self.provinces[r2][c].next_to(self.provinces[r][c2]): 
					#this probably shouldn't ever happen
					row += 'x'
				elif self.provinces[r2][c].next_to(self.provinces[r][c2]): 
					if color:  row += self.provinces[r2][c].connection_to(self.provinces[r][c2]).get_ansi()
					row += '/'
					if color: row += ANSI_RESET
				elif here.next_to(self.provinces[r2][c2]): 
					if color:  row += here.connection_to(self.provinces[r2][c2]).get_ansi()
					row += '\\'
					if color: row += ANSI_RESET
				else: row += ' '
			row += '\n'
			out += row
		return out

	def relax_points(self, steps=15):
		self.generate_voronoi()
		#olddiscrepancies = self.count_discrepancies()

		sim = ForceSim(self)
		for i in range(steps): sim.step()
		self.generate_voronoi()

		#newdiscrepancies = self.count_discrepancies()
		#print(olddiscrepancies, newdiscrepancies)

	def count_discrepancies(self):
		out = {'tp':0, 'fp':0, 'tn':0, 'fn':0}
		truepositive = set()
		positive = set()

		for province in self.get_all_provinces():
			for neighbor in province.get_neighbors():
				truepositive.add((province.gridpos, neighbor.gridpos))
				truepositive.add((neighbor.gridpos, province.gridpos))
		provinces = self.get_all_provinces()
		nprovinces = len(provinces)
		for point1, point2 in self.voronoi.ridge_points:
			positive.add((provinces[point1 % nprovinces].gridpos, provinces[point2 % nprovinces].gridpos))
			positive.add((provinces[point2 % nprovinces].gridpos, provinces[point1 % nprovinces].gridpos))

		out['tp'] = len(truepositive.intersection(positive))
		out['fp'] = len(positive - truepositive)
		out['fn'] = len(truepositive - positive)
		return out

	def generate_voronoi(self):
		provpoints = [prov.imagepos for prov in self.get_all_provinces()]
		provpoints = []
		for y_offset in (0, self.imshape[1], -self.imshape[1]):
			for x_offset in (0, self.imshape[0], -self.imshape[0]):
				provpoints.extend((np.array(prov.imagepos) + [y_offset, x_offset] for prov in self.get_all_provinces()))
		self.voronoi = Voronoi(provpoints)

	def relax_points_simple(self):
		newcenters = np.zeros((len(self.get_all_provinces()), 2))
		for provinceid, province in enumerate(self.get_all_provinces()):
			region = self.voronoi.regions[self.voronoi.point_region[provinceid]]
			vertices = [self.voronoi.vertices[vertid] for vertid in region if vertid != -1]
			y, x = np.int64(np.mean(vertices, axis=0))
			newcenters[provinceid] = [y % self.imshape[1], x % self.imshape[0]]
			province.imagepos = newcenters[provinceid]
		self.generate_voronoi()

	def prune_ridge(self, ridgeid):
		vertexids = self.voronoi.ridge_vertices[ridgeid]
		delete_vertexid = max(vertexids)
		keep_vertexid = min(vertexids)

		#.points: no action needed
		#.vertices: set lower vertex to midpoint, pop higher vertex
		self.voronoi.vertices[keep_vertexid] = np.mean([self.voronoi.vertices[keep_vertexid], self.voronoi.vertices[delete_vertexid]], axis=0)
		self.voronoi.vertices = np.vstack([self.voronoi.vertices[:delete_vertexid,:], self.voronoi.vertices[delete_vertexid+1:,:]])

		#.ridge_points: delete ridgeid
		self.voronoi.ridge_points = np.vstack([self.voronoi.ridge_points[:ridgeid,:], self.voronoi.ridge_points[ridgeid+1:,:]])
		#.ridge_vertices: delete ridgeid, replace delete_vertexid with keep_vertexid, decrement everything higher than vertexid
		self.voronoi.ridge_vertices.pop(ridgeid)
		for index, vertices in enumerate(self.voronoi.ridge_vertices):
			if delete_vertexid in vertices:
				vertices[vertices.index(delete_vertexid)] = keep_vertexid
			if vertices[0] > delete_vertexid: vertices[0] -= 1
			if vertices[1] > delete_vertexid: vertices[1] -= 1

		#.regions: pop deleted vertexid, decrement everything higher
		for region in self.voronoi.regions:
			if delete_vertexid in region: 
				if keep_vertexid in region: region.remove(delete_vertexid)
				else: region[region.index(delete_vertexid)] = keep_vertexid
			for index, vid in enumerate(region):
				if vid > delete_vertexid: region[index] -= 1
		#.point_region: no action needed
		#.furthest_site: no action needed

	def prune_ridges(self):
		finished = True
		provinces = self.get_all_provinces()
		for ridgeid, (pointid1, pointid2) in enumerate(self.voronoi.ridge_points):
			province1 = provinces[pointid1 % len(provinces)]
			province2 = provinces[pointid2 % len(provinces)]
			if not province1.next_to(province2):
				self.prune_ridge(ridgeid)
				finished = False
				break
		if not finished: self.prune_ridges()

	def adjust_ridges(self, threshold=0.4):

		return
		self.prune_ridges()

		return

		expand = set()
		lengths = {}
		delete = set()
		provinces = self.get_all_provinces()
		for ridgeid, (pointid1, pointid2) in enumerate(self.voronoi.ridge_points):
			province1 = provinces[pointid1 % len(provinces)]
			province2 = provinces[pointid2 % len(provinces)]

			vertexid1, vertexid2 = self.voronoi.ridge_vertices[ridgeid]
			if vertexid1 == -1 or vertexid2 == -1: continue

			vertex1 = self.voronoi.vertices[vertexid1]
			vertex2 = self.voronoi.vertices[vertexid2]

			length = np.linalg.norm(vertex2 - vertex1)

			if pointid1 not in lengths: lengths[pointid1] = {}
			if pointid2 not in lengths: lengths[pointid2] = {}
			if province1.next_to(province2):
				lengths[pointid1][ridgeid] = length
				lengths[pointid2][ridgeid] = length
			else: delete.add(ridgeid)

		#TODO: properly and actually delete the ridge so it doesn't cause problems down the line
		for ridgeid in delete[::-1]:
			vertexid1, vertexid2 = self.voronoi.ridge_vertices[ridgeid]
			newvertex = np.mean([self.voronoi.vertices[vertexid1], self.voronoi.vertices[vertexid2]], axis=0)

		for provinceid in lengths:
			medlength = np.median([lengths[provinceid][ridgeid] for ridgeid in lengths[provinceid]])
			minlength = np.min([lengths[provinceid][ridgeid] for ridgeid in lengths[provinceid]])
			if minlength < threshold * medlength: 
				for ridgeid in lengths[provinceid]:
					if lengths[provinceid][ridgeid] == minlength:
						expand.add(ridgeid)

		for ridgeid in expand:
			vertexid1, vertexid2 = self.voronoi.ridge_vertices[ridgeid]
			displacement = self.voronoi.vertices[vertexid2] - self.voronoi.vertices[vertexid1]
			self.voronoi.vertices[vertexid1] -= displacement * 0.6
			self.voronoi.vertices[vertexid2] += displacement * 0.6

		for ridgeid in delete:
			vertexid1, vertexid2 = self.voronoi.ridge_vertices[ridgeid]
			displacement = self.voronoi.vertices[vertexid2] - self.voronoi.vertices[vertexid1]
			self.voronoi.vertices[vertexid1] += displacement * 0.5
			self.voronoi.vertices[vertexid2] -= displacement * 0.5


	def __getitem__(self, index): return self.provinces.__getitem__(index)

class ForceSim(object):
	def __init__(self, grid):
		self.grid = grid
		self.ids_provinces = {}
		self.provinces_ids = {}

		self.rmsep = (self.grid.imshape[0] * self.grid.imshape[1] / np.pi / (self.grid.watersize * len(self.grid.get_water()) + len(self.grid.get_all_provinces()) - len(self.grid.get_water()))) ** 0.5

		self.initialize()

	def initialize(self):
		self.generate_lookup_table()
		self.get_grid_interactions()

	def generate_lookup_table(self):
		for provinceid, province in enumerate(self.grid.get_all_provinces()):
			self.ids_provinces[provinceid] = province
			self.provinces_ids[province.gridpos] = provinceid


	def get_grid_interactions(self):
		voronoineighbors = {}
		nprovinces = len(self.grid.get_all_provinces())
		for rawp1, rawp2 in self.grid.voronoi.ridge_points:
			p1 = rawp1 % nprovinces
			p2 = rawp2 % nprovinces
			if p1 in voronoineighbors: voronoineighbors[p1].add(p2)
			else: voronoineighbors[p1] = set([p2])
			if p2 in voronoineighbors: voronoineighbors[p2].add(p1)
			else: voronoineighbors[p2] = set([p1])

		self.interactions = []
		for provinceid1, province1 in enumerate(self.grid.get_all_provinces()):
			neighbors = province1.get_neighbors()

			nearby = self.grid.get_nearby(province1)
			for province2 in nearby:
				provinceid2 = self.lookup(province2)
				if province2 in neighbors: 
					if provinceid2 in voronoineighbors[provinceid1]:
						self.interactions.append({'provinces':(provinceid1, provinceid2), 'type':'atr'})
					else:
						self.interactions.append({'provinces':(provinceid1, provinceid2), 'type':'superatr'})
				else: 
					if provinceid2 in voronoineighbors[provinceid1]:
						self.interactions.append({'provinces':(provinceid1, provinceid2), 'type':'superrep'})
					else:
						self.interactions.append({'provinces':(provinceid1, provinceid2), 'type':'rep'})


	def lookup(self, index):
		if isinstance(index, int): return self.ids_provinces[index % len(self.ids_provinces)]
		elif isinstance(index, tuple): return self.provinces_ids[index]
		else: return self.provinces_ids[index.gridpos]

	def get_points(self):
		provpoints = [prov.imagepos for prov in self.grid.get_all_provinces()]
		provpoints = []
		for y_offset in (0, self.grid.imshape[1], -self.grid.imshape[1]):
			for x_offset in (0, self.grid.imshape[0], -self.grid.imshape[0]):
				provpoints.extend((np.array(prov.imagepos) + [y_offset, x_offset] for prov in self.grid.get_all_provinces()))
		return provpoints

	def step(self, scale=1):
		n = 0
		gridheight, gridwidth = self.grid.shape
		nprovinces = len(self.grid.get_all_provinces())
		forces = np.zeros((nprovinces, 2))
		provpoints = self.get_points()
		for interaction in self.interactions:
			provinceid1, provinceid2 = interaction['provinces']
			province1 = self.lookup(provinceid1)
			province2 = self.lookup(provinceid2)
			for repetitionid, (dr, dc) in enumerate(((0,0), (0,gridwidth), (0,-gridwidth), 
				(gridheight,0), (gridheight,gridwidth), (gridheight,-gridwidth), 
				(-gridheight,0), (-gridheight,gridwidth), (-gridheight,-gridwidth), )):
				pos2 = (province2.gridpos[0] + dr, province2.gridpos[1] + dc)
				if self.chebyshev(province1.gridpos, pos2) == 1:
					n += 1
					forces[provinceid1] += self.force(provpoints[provinceid1], provpoints[provinceid2 + repetitionid * nprovinces], interaction['type'])
					if province1.is_water() or province2.is_water:
						forces[provinceid1] += 0.5 * self.force(provpoints[provinceid1], provpoints[provinceid2 + repetitionid * nprovinces], 'rep')

		newpoints = []
		for pointid, point in enumerate(provpoints):
			force = forces[pointid % nprovinces] * scale
			newpoints.append(point + force * scale)
			
		for pointid, point in enumerate(newpoints):
			if pointid < nprovinces: self.lookup(pointid).imagepos = point

	def force(self, point1, point2, sign):
		#TODO: refine this thing. maybe use springs?
		if sign == 'superatr': direction = (point2 - point1) / np.linalg.norm(point2 - point1) ** 0.9
		elif sign == 'atr': direction = (point2 - point1) / np.linalg.norm(point2 - point1) ** 0.7
		elif sign == 'rep': direction = -(point2 - point1) / np.linalg.norm(point2 - point1) ** 1.1
		elif sign == 'superrep': direction = -(point2 - point1) / np.linalg.norm(point2 - point1) ** 1.4
		else: raise ValueError
		return direction

	def _lets_model_atoms_what_could_possibly_go_wrong_force(self, point1, point2, sign):
		if sign == 'superatr': direction = (point2 - point1) / np.linalg.norm(point2 - point1) ** 0.9
		elif sign == 'atr': direction = (point2 - point1) / np.linalg.norm(point2 - point1) ** 0.7
		elif sign == 'rep': direction = -(point2 - point1) / np.linalg.norm(point2 - point1) ** 1.1
		elif sign == 'superrep': direction = -(point2 - point1) / np.linalg.norm(point2 - point1) ** 1.4
		else: raise ValueError
		return direction

	def taxicab(self, arr1, arr2):
		return np.sum(np.abs([arr1[i] - arr2[i] for i in range(len(arr1))]))
	def chebyshev(self, arr1, arr2):
		return np.max(np.abs([arr1[i] - arr2[i] for i in range(len(arr1))]))

	def get_province_base_color(self, province):
		if province.is_water():
			if province.is_deep(): return 'darkblue'
			else: return 'blue'
		elif province.is_cave(): return 'purple'
		elif province.is_forest(): return 'green'
		elif province.is_highland(): return 'gray'
		elif province.is_mountain(): return 'white'
		elif province.is_waste(): return 'tan'
		elif province.is_farm(): return 'yellow'
		elif province.is_swamp(): return 'teal'
		else: return 'tab:green'
