import numpy as np

def gaussint(*args, **kwargs):
	return np.int64(np.round(np.random.normal(*args, **kwargs)))

#tree mask codes:
#0: blank/transparent
#1: summer outline
#2: summer leaves
#3-15: reserved
#16: blank/transparent
#17: winter outline (probably branches)
#18: winter leaves (probably empty)
#19: reserved
#20: snow
#grass mask codes
#32: blank/transparent
#33: summer/temperate (plains, forest)
#34: summer/moist (swamp)
#35: summer/dry (waste, highland)
#35: summer/sea (kelp)
#49: winter/temperate (plains, forest)
#50: winter/moist (swamp)
#51: winter/dry (waste, highland)
#52: winter/sea (kelp)
#mountain/hill mask codes
#65: summer mountain outline
#66: summer mountain fill
#67: summer mountain inline
#68: summer mountain snow
#73: summer hill outline
#74: summer hill fill
#97: winter mountain outline
#98: winter mountain fill
#99: winter mountain inline
#100: winter mountain snow
#105: winter hill outline
#106: winter hill fill

deciduous_palette = {
	0:[0x00,0x00,0x00,0x00],
	1:[0x77,0x66,0x44,0xff],
	2:[0x99,0xaa,0x66,0xff],
	16:[0x00,0x00,0x00,0xff],
	17:[0x77,0x66,0x44,0xff],
	18:[0x99,0xaa,0x66,0xff],

	65:[0x55, 0x55, 0x55, 0xff],
	66:[0xaa, 0xaa, 0xbb, 0xff],

	73:[0x88, 0x66, 0x55, 0xff],
	74:[0xdd, 0xcc, 0x99, 0xff],
}
evergreen_palette = {
	0:[0x00,0x00,0x00,0x00],
	1:[0x77,0x66,0x44,0xff],
	2:[0x88,0x99,0x66,0xff],
	16:[0x00,0x00,0x00,0xff],
	17:[0x77,0x66,0x44,0xff],
	18:[0x88,0x99,0x66,0xff],
}

class BaseSprite(object):
	def __init__(self, **kwargs):
		self.mask = kwargs.get('mask')
		self.image = kwargs.get('image')
		self.palette = kwargs.get('palette', deciduous_palette)

	def generate(self): raise NotImplementedError

	def colorize(self):
		r = np.zeros(self.mask.shape, dtype=np.uint8)
		g = np.zeros(self.mask.shape, dtype=np.uint8)
		b = np.zeros(self.mask.shape, dtype=np.uint8)
		a = np.zeros(self.mask.shape, dtype=np.uint8)

		for colorid in self.palette:
			color = self.palette[colorid]
			r[self.mask == colorid] = color[0]
			g[self.mask == colorid] = color[1]
			b[self.mask == colorid] = color[2]
			a[self.mask == colorid] = color[3]

		self.image = np.stack([r,g,b,a], axis=2)
		return self.image

	def winterize(self): raise NotImplementedError

class SimpleTree(BaseSprite):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.width = gaussint(9, 1) if kwargs.get('width') is None else kwargs.get('width')
		self.height = gaussint(13, 1) if kwargs.get('height') is None else kwargs.get('height')
		self.trunk = gaussint(3, 0.5) if kwargs.get('trunk') is None else kwargs.get('trunk')

		self.mask = kwargs.get('mask') if kwargs.get('mask') is not None else np.zeros((self.height, self.width), dtype=np.uint8)
		self.image = kwargs.get('image') if kwargs.get('image') is not None else np.zeros((self.height, self.width, 4), dtype=np.uint8)

		self.palette = kwargs.get('palette', deciduous_palette)

	def generate(self):
		self.mask = np.zeros((self.height, self.width), dtype=np.uint8)
		self.paint_leaves()
		self.paint_trunk()
		self.complete_outline()

		return self.colorize()

	def copy(self):
		return type(self)(width=self.width, height=self.height, trunk=self.trunk, mask=self.mask.copy(), image=self.image.copy())

	def create_winter(self):
		wintertree = self.copy()
		wintertree.winterize()

		return wintertree

	def paint_leaves(self):
		cuts = [(0, 0)]
		for r in range(self.mask.shape[0]-self.trunk):
			cuts.append(np.random.randint(0, int(np.ceil(self.mask.shape[1]/4)), 2))

		np.random.shuffle(cuts)
		for r in range(self.mask.shape[0]-self.trunk):
			cut = cuts[r]
			if r == 0:
				self.mask[r,cut[0]:self.mask.shape[1]-cut[1]] = 1
			else:
				self.mask[r,cut[0]:self.mask.shape[1]-cut[1]] = 2

				self.mask[r,cut[0]] = 1
				self.mask[r,self.mask.shape[1]-cut[1]-1] = 1

	def winterize(self):
		newmask = np.zeros(self.mask.shape, dtype=np.uint8)
		newmask[self.trunk:,:] = self.mask[self.trunk:,:]
		newmask[newmask == 2] = 0
		
		leaf = np.nonzero((self.mask == 2))
		leafcenter = np.mean(np.vstack(leaf), axis=1)
		trunkcol = np.nonzero(self.mask[-1] == 1)[0][0]
		leafoutline = np.vstack(np.nonzero((self.mask[:-self.trunk,:] == 1))).T
		outlineindices = np.arange(len(leafoutline))
		np.random.shuffle(outlineindices)

		chebyshev = lambda v1, v2: max(abs(v2 - v1))

		endpoints = []
		for outlineindex in outlineindices:
			v1 = leafoutline[outlineindex]
			tooclose = False
			for v2 in endpoints:
				if chebyshev(v1, v2) <= 1: 
					tooclose = True
					break
			if not tooclose: endpoints.append(v1)
				
		for endpoint in endpoints:
			adjcenter = np.array([np.mean([leafcenter[0], endpoint[0]]), leafcenter[1]])
			displacement = adjcenter - endpoint
			distance = np.linalg.norm(displacement)
			direction = displacement / distance
			for t in np.arange(0, distance-1, 1):
				point = np.int64(np.round(endpoint + direction * t))
				newmask[tuple(point)] = 17

		displacement = leafcenter - np.array([self.trunk, trunkcol])
		distance = np.linalg.norm(displacement)
		direction = displacement / distance
		for t in np.arange(0, distance, 0.5):
			point = np.int64(np.round(leafcenter + direction * t))
			newmask[tuple(point)] = 17

		newmask[newmask == 1] = 0
		newmask[newmask == 2] = 0
		newmask[self.trunk:,trunkcol] = 17

		self.mask = newmask
		self.colorize()


	def paint_trunk(self):
		self.mask[self.mask.shape[0] - self.trunk:self.mask.shape[0], self.mask.shape[1]//2] = 1

	def complete_outline(self):
		self.mask[(self.mask == 2) & ((np.roll(self.mask, 1, axis=0) == 0) | (np.roll(self.mask, -1, axis=0) == 0))] = 1

class OvalTree(SimpleTree):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.concavity = kwargs.get('concavity', 0.5)
	def paint_leaves(self):
		cuts = []
		widths = []
		widest = (self.mask.shape[0] - self.trunk) // 2 + gaussint(0, 1)
		for r in range(self.mask.shape[0] - self.trunk):
			if r == widest: 
				cuts.append((0, 0))
				widths.append(self.mask.shape[1])
			else:
				widths.append(max(1, int(round(self.mask.shape[1] * np.sin(r / (self.mask.shape[0] - self.trunk) * np.pi) ** self.concavity))))
				cuts.append(gaussint(0, 0.5, 2))
		for r, (cut, width) in enumerate(zip(cuts, widths)):
			left = max(0, int(round(self.mask.shape[1]/2 - width/2 + cut[0])))
			right = min(self.mask.shape[1]-1, int(round(self.mask.shape[1]/2 + width/2 + cut[1])))
			self.mask[r,left:right] = 2
			self.mask[r,[left,right]] = 1
		return self.mask

class SimpleEvergreenTree(SimpleTree):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.width = gaussint(8, 1) if kwargs.get('width') is None else kwargs.get('width')
		self.height = gaussint(12, 1) if kwargs.get('height') is None else kwargs.get('height')
		self.trunk = gaussint(2, 0.5) if kwargs.get('trunk') is None else kwargs.get('trunk')
		self.mask = kwargs.get('mask') if kwargs.get('mask') is not None else np.zeros((self.height, self.width), dtype=np.uint8)
		self.image = kwargs.get('image') if kwargs.get('image') is not None else np.zeros((self.height, self.width, 4), dtype=np.uint8)

		self.palette = kwargs.get('palette', evergreen_palette)

	def winterize(self): pass

class SimpleGrass(BaseSprite):
	def __init__(self, *args, **kwargs):
		self.width = gaussint(16, 1) if kwargs.get('width') is None else kwargs.get('width')
		self.height = gaussint(6, 0.5) if kwargs.get('height') is None else kwargs.get('height')

		self.minangle = -30
		self.maxangle = 30

		self.mask = kwargs.get('mask') if kwargs.get('mask') is not None else np.zeros((self.height, self.width), dtype=np.uint8)
		self.image = kwargs.get('image') if kwargs.get('image') is not None else np.zeros((self.height, self.width, 4), dtype=np.uint8)

		self.palette = kwargs.get('palette', deciduous_palette)

	def generate(self):
		self.mask = np.zeros((self.height, self.width), dtype=np.uint8)
		self.paint_blades()
		return self.colorize()

	def paint_blades(self):
		gaps = []
		maxtries = self.mask.shape[1]
		tries = 0
		while sum(gaps) < maxtries and tries < maxtries:
			gaps.append(max(1, gaussint(3, 1)))
			tries += 1


		basecol = gaps.pop() / 2
		step = (self.maxangle - self.minangle) / len(gaps) 
		angles = np.arange(self.minangle, self.maxangle + step, step)
		for bladeid, angle in enumerate(angles):
			height = max(1, int(round(self.mask.shape[0] * np.sin((bladeid) / len(angles) * np.pi) + np.random.normal(1, 0.5))))
			baseline = min(self.mask.shape[0], self.mask.shape[0] - gaussint(0.5, 0.5))

			jitter = np.random.normal(-0.025, 0.025)
			direction = np.array([-np.cos(angle * np.pi / 180 + jitter), np.sin(1 * angle * np.pi / 180 + jitter)]) 
			startpoint = np.array([baseline, basecol])
			for t in np.arange(0, height, 0.5):
				r, c = startpoint + t * direction
				if r >= self.mask.shape[0]: continue
				if c >= self.mask.shape[1]: continue
				if c < 0 or r < 0: continue
				self.mask[int(np.clip(round(r), 0, self.mask.shape[0]-1)), int(np.clip(round(c), 0, self.mask.shape[1]-1))] = 2
				
			try: basecol += gaps.pop()
			except IndexError: break

	def winterize(self): pass

class SimpleMountainChain(BaseSprite):
	def __init__(self, *args, **kwargs):
		self.width = kwargs.get('width', gaussint(16, 1))
		self.height = kwargs.get('height', gaussint(6, 0.5))
		self.points =  kwargs.get('points', [[0,0]])

		self.basewidth = kwargs.get('basewidth', 70)
		self.widthvariance = kwargs.get('widthvariance', 10)
		self.baseaspect = kwargs.get('baseaspect', 2.5)
		self.aspectvariance = kwargs.get('aspectvariance', 0.5)

		self.mask = kwargs.get('mask') if kwargs.get('mask') is not None else np.zeros((self.height, self.width), dtype=np.uint8)
		self.image = kwargs.get('image') if kwargs.get('image') is not None else np.zeros((self.height, self.width, 4), dtype=np.uint8)

		self.palette = kwargs.get('palette', deciduous_palette)
		self.mountains = []

	def generate(self):
		if len(self.points) == 1: 
			self.mountains.append(self.generate_mountain())
			self.mask = self.mountains[-1].mask
			self.image = self.mountains[-1].image

		else: pass

	def generate_mountain(self):
		width = np.random.normal(self.basewidth, self.widthvariance)
		height = width / np.random.normal(self.baseaspect, self.aspectvariance)
		mountain = SimpleMountain(
			width=int(round(width)),
			height=int(round(height)),
			palette=self.palette,
		)
		mountain.generate()
		return mountain

class SimpleMountain(BaseSprite):
	def __init__(self, *args, **kwargs):
		self.width = kwargs.get('width', gaussint(60, 10))
		self.height = kwargs.get('height', gaussint(30, 10))
		self.palette = kwargs.get('palette', deciduous_palette)
		self.mask = kwargs.get('mask', np.zeros((self.height, self.width), dtype=np.uint8))
		self.image = kwargs.get('image', np.zeros((self.height, self.width, 4), dtype=np.uint8))
		self.jaggies = kwargs.get('jaggies', gaussint(4, 1))

	def generate(self):
		
		peakcol = int(round(np.random.normal(self.width/2, self.width/10)))
		points = []
		points.extend([np.array([0,0]), np.array([self.height - 1, peakcol]), np.array([0, self.width - 1])])

		for _ in range(self.jaggies):
			self.displace_midpoint(points)


		for pointid in range(len(points) - 1):
			self.draw_line(points[pointid], points[pointid+1])

		self.complete_outline()

		#use upside down mask just to make the math easier to debug
		self.mask = self.mask[::-1,:]
		self.image = np.zeros((self.mask.shape + (4,)), dtype=np.uint8)
		self.colorize()
		return self.image

	def complete_outline(self):
		self.mask[((np.roll(self.mask, 1, 1) == 0) | (np.roll(self.mask, -1, 1) == 0)) & (self.mask == 66)] = 65

	def displace_midpoint(self, points):
		tries = 10
		scale = 0.5
		midrange = (points[0][1] + points[-1][1]) / 2
		while tries:
			#insert_after1 = np.random.randint(len(points) - 1)

			target = np.random.uniform(points[0][1], points[-1][1])
			distances = np.zeros(len(points) - 1)
			for pointindex in range(len(points)-1):
				distances[pointindex] = abs(np.mean([points[pointindex][1], points[pointindex+1][1]]) - target)
			insert_after = np.nonzero(distances == np.min(distances))[0][0]
			#insert_after = np.clip(gaussint(len(points)/2 - 1, scale), 0, len(points) - 2)

			point1 = points[insert_after]
			point2 = points[insert_after+1]

			midpoint = np.mean([point1, point2], axis=0)
			delta_y = abs(point2[0] - point1[0])
			midpoint[0] += np.random.uniform(-delta_y/3, delta_y/3)
			points.insert(insert_after+1, midpoint)
			
			break


	def add_jaggy(self, points):
		tries = 3
		while tries:
			insert_after = np.random.randint(len(points) - 1)
			point1 = points[insert_after]
			point2 = points[insert_after+1]

			if (point2[1] - point1[1]) < 4:
				tries -= 1
				continue
			elif (point2[1] - point1[1]) == 4:
				left = point1[1] + 1
				peak = left + 1
				right = left + 2

			else:
				distance = point2[1] - point1[1]
				midpoint = (point1[1] + point2[1]) / 2
				bias = 0.05 if insert_after < len(points)/2 else - 0.05
				peak = midpoint + gaussint(bias, distance * 0.1)
				left = point1[1] + gaussint(distance * 0.25, distance * 0.05)
				right = point2[1] - gaussint(distance * 0.25, distance * 0.05)
				#cols = np.arange(point1[1]+1, point2[1])
				#np.random.shuffle(cols)
				#left, peak, right = sorted(cols[:3])

			curheights = np.array([self.interpolate(point1, point2, col) for col in (left, peak, right)])
			heightoffsets = np.random.normal(curheights/8, curheights/16)
			heightoffsets[[0,2]] *= -0.5

			for c in range(3):
				points.insert(insert_after+1+c, np.array([np.clip(curheights[c]+heightoffsets[c], 0, self.mask.shape[0]-1), [left, peak, right][c]]))
				
			break

	def interpolate(self, point1, point2, col):
		return point1[0] + (point2[0] - point1[0]) / (point2[1] - point1[1]) * (col - point1[1] )


	def draw_line(self, point1, point2, resolution=0.75):
		direction = point2 - point1
		distance = np.linalg.norm(direction)
		for t in np.arange(0, distance + resolution, resolution):
			curpoint = np.int64(point1 + direction * t / distance)
			if (curpoint < 0).any(): continue
			elif (curpoint >= self.mask.shape).any(): continue
			self.mask[:curpoint[0],curpoint[1]] = 66
			self.mask[tuple(curpoint)] = 65 #FIXME: allocate palette space for mountain outlines
	
class SimpleHillChain(SimpleMountainChain):
	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.width = kwargs.get('width', gaussint(16, 1))
		self.height = kwargs.get('height', gaussint(6, 0.5))
		self.points =  kwargs.get('points', [[0,0]])

		self.basewidth = kwargs.get('basewidth', 60)
		self.widthvariance = kwargs.get('widthvariance', 10)
		self.baseaspect = kwargs.get('baseaspect', 4)
		self.aspectvariance = kwargs.get('aspectvariance', 0.5)

		self.mask = kwargs.get('mask') if kwargs.get('mask') is not None else np.zeros((self.height, self.width), dtype=np.uint8)
		self.image = kwargs.get('image') if kwargs.get('image') is not None else np.zeros((self.height, self.width, 4), dtype=np.uint8)

		self.palette = kwargs.get('palette', deciduous_palette)
		self.mountains = []

	def generate_mountain(self):
		width = np.random.normal(self.basewidth, self.widthvariance)
		height = width / np.random.normal(self.baseaspect, self.aspectvariance)
		mountain = SimpleHill(
			width=int(round(width)),
			height=int(round(height)),
			palette=self.palette,
		)
		mountain.generate()
		return mountain


class SimpleHill(SimpleMountain): 
	def __init__(self, *args, **kwargs):
		self.width = kwargs.get('width', gaussint(60, 10))
		self.height = kwargs.get('height', gaussint(15, 5))
		self.palette = kwargs.get('palette', deciduous_palette)
		self.mask = kwargs.get('mask', np.zeros((self.height, self.width), dtype=np.uint8))
		self.image = kwargs.get('image', np.zeros((self.height, self.width, 4), dtype=np.uint8))
		self.jaggies = kwargs.get('jaggies', gaussint(6, 1))

	def colorize(self):
		self.mask[self.mask == 65] = 73
		self.mask[self.mask == 66] = 74
		super().colorize()

def preview(arr):
	import matplotlib.pyplot as plt
	plt.imshow(arr)
	plt.show()
