import numpy as np

class PoissonDiskSampler(object):
	def __init__(self, shape, radius=None, count=None, points=None, radiusmap=None, densitymap=None, wrap=(1,1), tries=30):
		if radius is None and count is None and radiusmap is None and densitymap is None: 
			raise TypeError('Not enough information to place points. Specify radius, count, radiusmap, or densitymap.')

		elif radius is not None and radiusmap is not None:
			raise TypeError('radius conflicts with radiusmap')

		elif radius is not None and count is not None:
			raise TypeError('radius conflicts with count')

		elif radius is not None and count is not None:
			raise TypeError('count conflicts with radiusmap')

		elif radiusmap is not None and densitymap is not None:
			raise TypeError('radiusmap conflicts with densitymap')

		self.radius = radius
		self.shape = shape 
		self.count = count
		self.points = np.zeros((0, 2)) if points is None else points
		self.radiusmap = radiusmap
		self.densitymap = densitymap

		self.wrap = wrap

		self.tries = tries

		self.grid = None
		self.gridspacing = None

	def generate(self, skip_invalid=False, conflict_resolution='current'):
		if self.radius and self.densitymap is None:
			self.gridspacing = self.radius / len(self.shape) ** 0.5
			self.grid = -np.ones(tuple(int(np.ceil(self.shape[i]/self.gridspacing)) for i in range(len(self.shape))), dtype=np.int64)
			R2 = self.radius ** 2
			N2 = 2
			points = []
			queue = []
			if len(self.points):
				for point in self.points:
					self.sanitize_point(point)
					gridpos = self.to_grid(point)
					if self.grid[gridpos] != -1: 
						if skip_invalid: continue
						else: raise IndexError('Collision between manually specified points!')
					elif self.too_close(point, points, R2, N2): 
						if skip_invalid: continue
						else: raise IndexError('Collision between manually specified points!')

					self.grid[gridpos] = len(points)
					queue.append(len(points))
					points.append(point)
			else:
				point = np.random.uniform(self.shape)
				self.grid[self.to_grid(point)] = len(points)
				queue.append(len(points))
				points.append(point)

			while queue:
				success = False
				originindex = np.random.randint(len(queue))
				origin = points[queue[originindex]]
				for attempt in range(self.tries):
					radius = self.radius * np.random.uniform(1, 2)
					angle = np.random.uniform(2 * np.pi)
					point = origin + radius * np.array([np.cos(angle), np.sin(angle)])

					self.sanitize_point(point)
					gridpos = self.to_grid(point)

					if self.too_close(point, points, R2, N2): continue
					else:
						self.grid[gridpos] = len(points)
						queue.append(len(points))
						points.append(point)
						success = True
						break

				if not success: queue.pop(originindex)

			self.points = np.array(points)

		elif self.radius and self.densitymap is not None:
			if np.max(self.densitymap) == 0:
				self.points = []
				return
			self.gridspacing = self.radius / np.max(self.densitymap) / len(self.shape) ** 0.5
			self.grid = -np.ones(tuple(int(np.ceil(self.shape[i]/self.gridspacing)) for i in range(len(self.shape))), dtype=np.int64)


			R2_1 = self.radius ** 2
			#print(R2_1 / np.min(self.densitymap), R2_1 / np.max(self.densitymap))
			points = []
			queue = []
			if len(self.points):
				for point in self.points:
					self.sanitize_point(point)

					pointdensity = self.densitymap[tuple(np.int64(point))]
					if pointdensity == 0:
						if skip_invalid: continue
						else: raise IndexError('Manually specified point is in a region with 0 density!')
					gridpos = self.to_grid(point)
					R2 = R2_1 / self.densitymap[tuple(np.int64(point))]
					N2 = min(max(2, 2 * ((pointdensity/self.radius)**2)), max(self.grid.shape)//2)
					if self.grid[gridpos] != -1: 
						if skip_invalid: continue
						else: raise IndexError('Collision between manually specified points!')
					elif self.too_close(point, points, R2, N2): 
						if skip_invalid: continue
						else: raise IndexError('Collision between manually specified points!')

					self.grid[gridpos] = len(points)
					queue.append(len(points))
					points.append(point)
			else:
				valid = np.vstack(np.nonzero(self.densitymap)).T
				point = valid[np.random.randint(len(valid))].as_float()
				self.grid[self.to_grid(point)] = len(points)
				queue.append(len(points))
				points.append(point)

			while queue:
				success = False
				originindex = np.random.randint(len(queue))
				origin = points[queue[originindex]]
				origindensity = self.densitymap[tuple(np.int64(origin))] ** 2
				for attempt in range(self.tries):
					radius = self.radius * np.random.uniform(1, 2)
					angle = np.random.uniform(2 * np.pi)
					point = origin + radius * np.array([np.cos(angle), np.sin(angle)])

					self.sanitize_point(point)
					pointdensity = self.densitymap[tuple(np.int64(point))] ** 2
					if pointdensity == 0: continue
					#R2 = R2_1 / min(origindensity, pointdensity)
					if conflict_resolution == 'current':
						R2 = R2_1 / pointdensity
						N2 = min(max(2, 2 * (pointdensity/self.radius) ** -2), max(self.grid.shape)//2)
					elif conflict_resolution == 'prior':
						R2 = R2_1 / origindensity
						N2 = min(max(2, 2 * (origindensity/self.radius) ** -2), max(self.grid.shape)//2)
					gridpos = self.to_grid(point)

					if self.too_close(point, points, R2, N2): continue
					else:
						self.grid[gridpos] = len(points)
						queue.append(len(points))
						points.append(point)
						success = True
						break

				if not success: queue.pop(originindex)

			self.points = np.array(points)

	def too_close(self, point, points, R2, N2):
		gridpos = self.to_grid(point)
		if self.grid[gridpos] != -1: return True

		#if (2 * N2**2) < len(points):
		if (2 * N2**2) < len(points):

			for dr in range(-N2, N2+1):
				cur_r = gridpos[0] + dr
				if self.wrap[0]: cur_r = cur_r % self.grid.shape[0]
				elif 0 <= cur_r < self.grid.shape[0]: pass
				else: continue

				for dc in range(-N2, N2+1):
					if not (dr or dc): continue
					if (dr ** 2 + dc ** 2) > N2: continue

					closest = max(0, abs(dr)-1)**2 + max(0, abs(dc)-1)**2
					if closest > (2 * N2**2): continue

					cur_c = gridpos[1] + dc
					if self.wrap[1]: cur_c = cur_c % self.grid.shape[1]
					elif 0 <= cur_c < self.grid.shape[1]: pass
					else: continue

					if self.grid[cur_r, cur_c] == -1: continue

					neighbor = points[self.grid[cur_r, cur_c]]

					sqdist = self.sqdistance(point, neighbor)
					if sqdist < R2: return True
		#with extreme density disparities, the grid doesn't actually help
		else:
			for neighbor in points:
				sqdist = self.sqdistance(point, neighbor)
				if sqdist < R2: return True

		return False

	def sqdistance(self, point1, point2):
		sqdist = 0
		for i, wrap in enumerate(self.wrap):
			if wrap:
				sqdist += min(abs(point2[i] - point1[i]), abs(self.shape[i] - abs(point2[i] - point1[i]))) ** 2
			else:
				sqdist += abs(point2[i] - point1[i]) ** 2
		return sqdist

	def sanitize_point(self, point):
		for i, wrap in enumerate(self.wrap):
			if wrap: point[i] = point[i] % self.shape[i]
			elif 0 <= point[i] < self.shape[i]: pass
			else: raise IndexError('{} is out of bounds'.format(point))
		return point

	def to_grid(self, point): return tuple(np.int64(point // self.gridspacing))

