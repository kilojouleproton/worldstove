#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

def render(grid):
	fig, ax = plt.subplots()

	landX = []
	landY = []
	waterX = []
	waterY = []
	for r in range(grid.shape[0]):
		for c in range(grid.shape[1]):
			province = grid[r][c]
			if province.is_water():
				waterX.append(province.imagepos[0])
				waterY.append(province.imagepos[1])
			else:
				landX.append(province.imagepos[0])
				landY.append(province.imagepos[1])
	ax.imshow(np.zeros(grid.imshape))
	ax.plot(waterX, waterY, lw=0, marker='.')
	ax.plot(landX, landY, lw=0, marker='.')

	plt.show()
	plt.close()
