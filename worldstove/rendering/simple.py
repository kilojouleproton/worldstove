import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.patches as patches
import time
import worldstove.noise as noise
import worldstove.spritegen as spritegen
import cv2

simple_palette = {
	#5588aa or 6688aa
	'freshwater': (0x88, 0xaa, 0xdd),
	'rivershore': (0x88, 0x99, 0xaa),
	'sea': (0x55, 0x88, 0xaa),
	'deepsea': (0x44, 0x77, 0x99),
	'swamp': (0x77, 0x66, 0x33),
	'forest': (0x99, 0xaa, 0x77),
	'waste': (0xee, 0xaa, 0x66),
	'plain': (0xaa, 0xaa, 0x66),
	'cave': (0x88, 0x88, 0x88),
	'highlands': (0xaa, 0x99, 0x44),
	'farm': (0xff, 0xee, 0x99),

	'provinceborder': (0x33, 0x33, 0x33),
	'bridge': (0x99, 0x88, 0x44),
	'road': (0xee, 0xcc, 0x88),
	'debugsprite': (0xff, 0x00, 0xff),
}

class SimpleRenderer(object):
	def __init__(self, grid):
		self.grid = grid
		self.ids_provinces = {}
		self.provinces_ids = {}
		self.generate_lookup_table()

	def render(self):
		self.canvas = np.zeros(self.grid.imshape + (3,), dtype=np.uint8)

		self.fill(self.canvas, color=(255,0,255))
		self.points = np.vstack(np.nonzero(self.canvas[:,:,0])).T

		self.paint_base_terrain()
		self.paint_connections()

		self.generate_simple_sprites()
		self.place_sprites()

		#plt.imshow(self.canvas, origin='lower')
		plt.imshow(self.canvas)
		#for province in self.grid.get_all_provinces():
			#plt.annotate('={}'.format(province.terrain), [province.imagepos[0], self.grid.imshape[0] - province.imagepos[1]])
			#plt.annotate('={}'.format(province.terrain), [province.imagepos[0], province.imagepos[1]])
			#plt.scatter(province.imagepos[0], province.imagepos[1], color='r', marker='x')
		#self.preview()
		plt.show()

	def generate_simple_sprites(self, samples=64):
		self.sprites = {}
		targets = {
			'hill': spritegen.SimpleHillChain,
			'mountain': spritegen.SimpleMountainChain,
			'grass': spritegen.SimpleGrass,
			'tree_deciduous': spritegen.OvalTree,
			'tree_evergreen': spritegen.SimpleEvergreenTree,
		}
		for name in targets:
			self.sprites[name] = []
			gen = targets[name]()
			for _ in range(samples):
				gen.generate()
				self.sprites[name].append(gen.image)
		self.sprites['tree_deciduous'].extend(self.sprites['tree_evergreen'])

	def place_sprites(self):
		paintqueue = []

		rawdensitymaps = {}
		densitymaps = {}
		polygons = {}
		initpoints = {}

		watermask = np.zeros(self.grid.imshape[:2], dtype=np.uint8)
		waterpolys = []

		for name in self.sprites:
			rawdensitymaps[name] = np.zeros(self.grid.imshape[:2], dtype=np.uint8)
			polygons[name] = {'points': {}, 'color': 1}
			initpoints[name] = []

		provinces = self.grid.get_all_provinces()
		#TODO: wrap this block into some method like self.get_polygons()
		for pointid, point in enumerate(self.grid.voronoi.points):
			provinceid = pointid % len(provinces)
			if provinceid not in polygons: polygons[provinceid] = {'color':None, 'points':[]}
			province = provinces[provinceid]
			region = self.grid.voronoi.regions[self.grid.voronoi.point_region[pointid]]

			name = None
			weight = 255
			if province.is_water(): 
				waterpolys.append(np.int32([self.grid.voronoi.vertices[vertid] for vertid in region if vertid != -1]))
			else:
				if province.is_highland(): name = 'hill'
				elif province.is_forest(): name = 'tree_deciduous'
				elif province.is_plain(): 
					name = 'tree_deciduous'
					weight = 127
			if name is not None:
				if weight not in polygons[name]['points']: polygons[name]['points'][weight] = []
				polygons[name]['points'][weight].append(np.int32([self.grid.voronoi.vertices[vertid] for vertid in region if vertid != -1]))


		cv2.fillPoly(watermask, waterpolys, 1)
		epsilon = 0.00
		for name in rawdensitymaps:
			for weight in polygons[name]['points']:
				cv2.fillPoly(rawdensitymaps[name], polygons[name]['points'][weight], weight)

		for name in rawdensitymaps:
			copied = np.hstack([np.vstack([rawdensitymaps[name]] * 3)] * 3)
			
			blurred = np.zeros_like(copied)
			cv2.GaussianBlur(copied, (127, 127), 31, blurred)
			del copied
			origshape = rawdensitymaps[name].shape
			densitymaps[name] = np.float64(blurred[origshape[0]:origshape[0] * 2, origshape[1]:origshape[1] * 2]) / 255
			densitymaps[name][watermask == 1] = 0


		
		for pointid, point in enumerate(self.grid.voronoi.points):
			sortkey = (point[1], point[0])
			spritepoint = point + np.random.normal(4, 4, (2,))
			province = self.lookup(pointid)
			if not province.is_water():
				if province.is_highland():
					#paintqueue.append((sortkey, spritepoint, np.random.randint(len(self.sprites['hill']))))
					initpoints['hill'].append(spritepoint)
				if province.is_forest():
					#paintqueue.append((sortkey, spritepoint, np.random.randint(len(self.sprites['tree_deciduous']))))
					initpoints['tree_deciduous'].append(spritepoint)

		radii = {'hill': 64,
			'mountain': 64,
			'grass': 16,
			'tree_deciduous': 8,
			'tree_evergreen': 8,
		}

		paintqueue = []
		for name in densitymaps:
			pds = noise.PoissonDiskSampler(shape=self.canvas.shape[:2], radius=radii[name], densitymap=densitymaps[name], points=initpoints[name])
			pds.generate(skip_invalid=True)
			for point in pds.points:
				sortkey = (point[0], point[1])
				sprite = self.sprites[name][np.random.randint(len(self.sprites[name]))]
				point = np.int32(point)
				paintqueue.append((sortkey, point, sprite))

		paintqueue.sort()
		for sortkey, point, sprite in paintqueue:
			self.blit(self.canvas, point, sprite)

	def blit(self, arr, point, sprite, halign='center', valign='bottom', wrap=(1, 1)):
		topleft = [0, 0]
		if halign == 'left':
			topleft[1] = point[1]
		elif halign == 'center':
			topleft[1] = point[1] - sprite.shape[1]//2
		elif halign == 'right':
			topleft[1] = point[1] - sprite.shape[1]

		if valign == 'top':
			topleft[0] = point[0]
		elif valign == 'center':
			topleft[0] = point[0] - sprite.shape[0]//2
		elif valign == 'bottom':
			topleft[0] = point[0] - sprite.shape[0]

		for dx in range(sprite.shape[1]):
			x = topleft[1] + dx
			if wrap[1]: x = x % arr.shape[1]
			if not (0 <= x < self.grid.imshape[1]): pass

			for dy in range(sprite.shape[0]):
				if sprite[dy,dx,3] == 0: continue
				y = topleft[0] + dy
				if wrap[0]: y = y % arr.shape[0]
				if not (0 <= y < self.grid.imshape[0]): pass
				arr[y,x,:3] = sprite[dy,dx,:3]

	def paint_base_terrain(self):
		provinces = self.grid.get_all_provinces()
		polygons = {}
		for pointid, point in enumerate(self.grid.voronoi.points):
			provinceid = pointid % len(provinces)
			if provinceid not in polygons: polygons[provinceid] = {'color':None, 'points':[]}
			province = provinces[provinceid]
			region = self.grid.voronoi.regions[self.grid.voronoi.point_region[pointid]]

			polygons[provinceid]['color'] = self.get_province_base_color(province, mode='int')
			polygons[provinceid]['points'].append(np.int32([self.grid.voronoi.vertices[vertid] for vertid in region if vertid != -1]))

		for provinceid in polygons:
			cv2.fillPoly(self.canvas, polygons[provinceid]['points'], polygons[provinceid]['color'])

	def paint_connections(self):
		lines = {-1:[], 0:[], 1:[], 2:[], -2:[], 3:[], 4:[], 8:[]}
		for ridgeid, (pointid1, pointid2) in enumerate(self.grid.voronoi.ridge_points):
			province1 = self.lookup(pointid1)
			province2 = self.lookup(pointid2)
			sortkey = tuple(-self.grid.voronoi.points[pointid1] - self.grid.voronoi.points[pointid2])[::-1]
			#point1 = np.int32(self.voronoi.points[pointid1])
			#point2 = np.int32(self.voronoi.points[pointid2])

			if province1.next_to(province2):
				connection = self.grid.get_connection(province1, province2)
				lines[connection.special].append((sortkey, ridgeid))
			lines[-1].append((sortkey, ridgeid))

		for special in lines:
			lines[special].sort()
			for sortkey, ridgeid in lines[special]:
				province1 = self.lookup(pointid1)
				province2 = self.lookup(pointid2)

				points = [np.int32(self.grid.voronoi.points[pointid]) for pointid in self.grid.voronoi.ridge_points[ridgeid] if pointid != -1]
				vertices = [np.int32(self.grid.voronoi.vertices[vertexid]) for vertexid in self.grid.voronoi.ridge_vertices[ridgeid] if vertexid != -1]
				if len(vertices) < 2: continue
				if special == -1: 
					cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['provinceborder'], 1, cv2.LINE_4)
				elif special == 2:
					cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['rivershore'], 8, cv2.LINE_4)
					cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['freshwater'], 4, cv2.LINE_4)
				elif special == -2:
					cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['rivershore'], 8, cv2.LINE_4)
					cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['freshwater'], 4, cv2.LINE_4)

					displacement = vertices[1] - vertices[0]
					distance = np.linalg.norm(displacement)
					direction = displacement / distance
					normal = np.array([direction[1], -direction[0]])
					t = np.random.normal(0.5, 0.1)
					bridgepoint = vertices[0] + displacement * t

					#FIXME: replace with procedural bridge sprites
					cv2.line(self.canvas, tuple(np.int32(bridgepoint - normal * 16)), tuple(np.int32(bridgepoint + normal * 16)), simple_palette['bridge'], 16)

				elif special == 4:
					#need to pass scale parameter somehow for high/low-res maps
					SCALE = 16

					displacement = vertices[1] - vertices[0]
					distance = np.linalg.norm(displacement)

					normal = displacement[::-1] * [1., -1]
					normal /= distance
					
					for _ in range(int(distance / SCALE * 2)):
						anchor = np.random.random()
						dist = np.random.normal(0, SCALE/2)
						#dist = 0
						cv2.drawMarker(self.canvas, tuple(np.int32(vertices[0] + anchor * displacement + dist * normal)), simple_palette['debugsprite'], cv2.MARKER_TRIANGLE_UP, 32, 4, cv2.FILLED) #also need to figure out the correct orientation

				elif special == 8:
					cv2.line(self.canvas, tuple(points[0]), tuple(points[1]), simple_palette['road'], 8, cv2.LINE_4)
					#cv2.line(self.canvas, tuple(vertices[0]), tuple(vertices[1]), simple_palette['road'], 4, cv2.LINE_4)

		cv2.line(self.canvas, (100, 100), (500, 200), (0, 255, 0), 2, cv2.FILLED)


	def fill(self, arr, index=(None,None), color=(0, 0, 0)):
		r, g, b = arr[:,:,0], arr[:,:,1], arr[:,:,2]
		r[index] = color[0]
		g[index] = color[1]
		b[index] = color[2]

		arr = np.stack([r, g, b], axis=2)

	def preview(self):
		fig, ax = plt.subplots()
		ax.imshow(self.canvas)
		self.preview_base_terrain(ax=ax)
		self.preview_special_connections(ax=ax)

		#voronoi_plot_2d(self.voronoi, ax=ax)
		ax.set_xlim([0, self.grid.imshape[1]])
		ax.set_ylim([0, self.grid.imshape[0]])

		#ax.scatter(newcenters[:,0], newcenters[:,1], marker='x', color='r', zorder=5)
		self.preview_connections(ax=ax)
		self.preview_special(ax=ax)

		#fig.savefig('test_out/{}.png'.format(time.time()))
		#plt.close()
		#plt.show()
	
	def preview_connections(self, ax):
		seen = set()
		for province in self.grid.get_all_provinces():
			provinceid = self.lookup(province)
			for neighbor in province.get_neighbors():
				neighborid = self.lookup(neighbor)
				if (provinceid, neighborid) in seen: continue
				seen.add((neighborid, provinceid))
				if self.chebyshev(province.gridpos, neighbor.gridpos) <= 1:
					ax.plot([p.imagepos[0] for p in (province, neighbor)], [p.imagepos[1] for p in (province, neighbor)], color='w', alpha=0.5)

	def preview_special(self, ax):
		starts = [province.imagepos for province in self.grid.get_all_provinces() if province.startsite]
		thrones = [province.imagepos for province in self.grid.get_all_provinces() if province.thronesite]

		ax.plot([p[0] for p in starts], [p[1] for p in starts], lw=0, color='r', marker='p', markersize=20)
		ax.plot([p[0] for p in thrones], [p[1] for p in thrones], lw=0, color='m', marker='*', markersize=20)


	def generate_lookup_table(self):
		for provinceid, province in enumerate(self.grid.get_all_provinces()):
			self.ids_provinces[provinceid] = province
			self.provinces_ids[province.gridpos] = provinceid

	def lookup(self, index):
		if isinstance(index, int) or isinstance(index, np.int32): return self.ids_provinces[index % len(self.ids_provinces)]
		elif isinstance(index, tuple): return self.provinces_ids[index]
		else: return self.provinces_ids[index.gridpos]

	def chebyshev(self, arr1, arr2):
		return np.max(np.abs([arr1[i] - arr2[i] for i in range(len(arr1))]))

	def preview_base_terrain(self, ax):
		provinces = self.grid.get_all_provinces()
		for pointid, point in enumerate(self.grid.voronoi.points):
			province = provinces[pointid % len(provinces)]
			region = self.grid.voronoi.regions[self.grid.voronoi.point_region[pointid]]
			vertices = [self.grid.voronoi.vertices[vertid] for vertid in region if vertid != -1]
			fc = self.get_province_base_color(province)
			polygon = patches.Polygon(vertices, fc=fc)
			ax.add_patch(polygon)
			
	def preview_special_connections(self, ax):
		special = {}
		for conn in self.grid.connections:
			if conn.special:
				provinceid1 = self.lookup(conn.provinces[0])
				provinceid2 = self.lookup(conn.provinces[1])
				special[provinceid1, provinceid2] = conn.special
				special[provinceid2, provinceid1] = conn.special
		for ridgeid, (pointid1, pointid2) in enumerate(self.grid.voronoi.ridge_points):
			if (pointid1, pointid2) in special:
				sptype = special[pointid1, pointid2]
				vertexid1, vertexid2 = self.grid.voronoi.ridge_vertices[ridgeid]
				vertex1 = self.grid.voronoi.vertices[vertexid1]
				vertex2 = self.grid.voronoi.vertices[vertexid2]

				displacement = vertex2 - vertex1
				normal = displacement[::-1]

				if sptype == 1: #mountain pass
					passx = np.random.uniform(0.4, 0.6)
					passwidth = np.random.uniform(0.35, 0.65)

					mountains = np.concatenate([
						np.arange(0, passx - passwidth/2, 0.05),
						np.arange(passx + passwidth/2, 1, 0.05),
					])
					noise = np.random.normal(0, 0.05, size=len(mountains))
					ax.plot(vertex1[0] + displacement[0] * mountains + noise * normal[0], 
						vertex1[1] + displacement[1] * mountains + noise * normal[1], 
						lw=0, color='k', zorder=5, marker='^'
					)
				elif sptype == -2: #bridged river
					ax.plot([vertex1[0], vertex2[0]], [vertex1[1], vertex2[1]], color='tab:blue', lw=8, zorder=5)
					bridgepos = np.random.uniform(0.3, 0.7)
					normnormal = normal / np.linalg.norm(normal)
					ax.plot([
						vertex1[0] + displacement[0] * bridgepos - normnormal[0] * 16,
						vertex1[0] + displacement[0] * bridgepos + normnormal[0] * 16,
					], [
						vertex1[1] + displacement[1] * bridgepos + normnormal[1] * 16,
						vertex1[1] + displacement[1] * bridgepos - normnormal[1] * 16,
					], lw=4, color='brown', zorder=5)
				elif sptype == 2: #river
					ax.plot([vertex1[0], vertex2[0]], [vertex1[1], vertex2[1]], color='tab:blue', lw=8, zorder=5)
				elif sptype == 4: #unwalkable
					mountains = np.arange(0, 1, 0.05)
					noise = np.random.normal(0, 0.05, size=len(mountains))
					ax.plot(vertex1[0] + displacement[0] * mountains + noise * normal[0], 
						vertex1[1] + displacement[1] * mountains + noise * normal[1], 
						lw=0, color='k', zorder=5, marker='^',
						markersize=8,
					)
				elif sptype == 8: #road
					ax.plot(
						[self.grid.voronoi.points[pid][0] for pid in (pointid1, pointid2)],
						[self.grid.voronoi.points[pid][1] for pid in (pointid1, pointid2)],
						lw=4, color='brown'
					)

	def get_province_base_color(self, province, mode='str'):
		if mode == 'str':
			if province.is_water():
				if province.is_deep(): return 'darkblue'
				else: return 'blue'
			elif province.is_cave(): return 'purple'
			elif province.is_forest(): return 'green'
			elif province.is_highland(): return 'gray'
			elif province.is_mountain(): return 'white'
			elif province.is_waste(): return 'tan'
			elif province.is_farm(): return 'yellow'
			elif province.is_swamp(): return 'teal'
			else: return 'tab:green'
		else:
			palette = simple_palette
			if province.is_water():
				if province.is_deep(): return palette['deepsea']
				elif province.is_freshwater(): return palette['freshwater']
				else: return palette['sea']
			elif province.is_cave(): return palette['cave']
			elif province.is_forest(): return palette['forest']
			elif province.is_highland(): return palette['highlands']
			elif province.is_mountain(): return palette['highlands']
			elif province.is_waste(): return palette['waste']
			elif province.is_farm(): return palette['farm']
			elif province.is_swamp(): return palette['swamp']
			else: return palette['plain']
			

def render(grid):
	x = SimpleRenderer(grid)
	return x.render()
	fig, ax = plt.subplots()

	landX = []
	landY = []
	waterX = []
	waterY = []
	for r in range(grid.shape[0]):
		for c in range(grid.shape[1]):
			province = grid[r][c]
			if province.is_water():
				waterX.append(province.imagepos[0])
				waterY.append(province.imagepos[1])
			else:
				landX.append(province.imagepos[0])
				landY.append(province.imagepos[1])
	ax.imshow(np.zeros(grid.imshape))
	ax.plot(waterX, waterY, lw=0, marker='.')
	ax.plot(landX, landY, lw=0, marker='.')

	plt.show()
	plt.close()
