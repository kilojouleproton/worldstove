import numpy as np

'''

IMPORTANT NOTE ON INDEXING CONVENTIONS:

GRID coordinates are (Y, X) just like arrays everywhere
VISUAL coordinates are (X, Y)
'''

class Point(object):
	coords = None
	def __init__(self, *args, **kwargs):
		self.coords = kwargs.get('coords', np.array([np.nan, np.nan]))

	def __getitem__(self, index): return self.coords.__getitem__(index)
	def __setitem__(self, index, newval): self.coords.__setitem__(index, newval)

	def __add__(self, other): return self.coords.__add__(other)
	def __sub__(self, other): return self.coords.__sub__(other)
	def __mul__(self, other): return self.coords.__mul__(other)
	def __div__(self, other): return self.coords.__div__(other)

class Edge(object):
	points = None
	def __init__(self, *args, **kwargs):
		self.points = kwargs.get('points', [])

	def split(self, xy, width, height): 
		disp = np.abs(self.points[1,:] - self.points[0,:])
		splitme = [False, False]
		if disp[0] > width/2: splitme[0] = True
		if disp[1] > height/2: splitme[1] = True

		if not (splitme[0] or splitme[1]):
			return [self]

		else: raise NotImplementedError

	def __getitem__(self, index): return self.points.__getitem__(index)
	def __setitem__(self, index, newval): self.points.__setitem__(index, newval)

	@staticmethod
	def trace_polygon(vertices, closed=True):
		start = -1 if closed else 0
		edges = []
		for i in range(start, len(vertices)-1):
			edges.append(Edge(points=vertices[i:i+2]))
		return edges

class Area(object):
	vertices = None
	
	def __init__(self, *args, **kwargs):
		self.vertices = kwargs.get('vertices', [])
		self.edges = kwargs.get('edges', [])

		if len(self.vertices) >= 2 and not self.edges:
			self.edges = Edge.trace_polygon(self.vertices, closed=True)
