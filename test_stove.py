#!/usr/bin/env python

import numpy as np
import worldstove
import unittest
import burner


class TestBasicGrid(unittest.TestCase):
	shape = (100, 200)
	box = np.array([
		[20, 20],
		[20, 40],
		[40, 40],
		[40, 20],
	])

	wrap = np.array([
		[20, 20],
		[20, 180],
		[40, 10],
	])

	def test_edge_from_array(self):
		worldstove.geometry.Edge(self.box[:2,:])

#class TestBurner3(unittest.TestCase):
class TestBurner3(object):
	nations = ['ma_arco', 'ma_atlantis', 'cavestart']

	def test_basic_m3(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '3',
			'--playerprovinces', '16',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)

	def test_landonly_m3(self):
		nations = ['ma_arco', 'foreststart', 'cavestart']
		args = burner.parser.parse_args(nations + [
			'--thrones', '3',
			'--playerprovinces', '16',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)


class TestBurner5(unittest.TestCase):
	nations = ['ma_ermor', 'la_rlyeh', 'ea_agartha', 'ea_mictlan', 'ea_ubar']

	def dont_test_basic_xs5(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '5',
			'--playerprovinces', '10',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)
	def dont_test_basic_s5(self):
		args = burner.parser.parse_args(self.nations + [
			'5', 
			'--thrones', '5',
			'--playerprovinces', '13',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)
	def test_basic_m5(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '5',
			'--playerprovinces', '16',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)
	def test_basic_l5(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '5',
			'--playerprovinces', '19',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)
	def dont_test_basic_xl5(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '5',
			'--playerprovinces', '22',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)
	def dont_test_basic_xxl5(self):
		args = burner.parser.parse_args(self.nations + [
			'--thrones', '5',
			'--playerprovinces', '25',
			'--hspacing', '180',
			'--vspacing', '120',
		])
		burner.main(args)

class TestBigWater5(TestBurner5):
	nations = ['ma_atlantis', 'la_rlyeh', 'ma_ys', 'ea_mictlan', 'ea_ubar']

class TestNukeParser(unittest.TestCase):
	def test_basic(self):
		worldstove.nukeparser.load_all('Nations')

	def test_find_nation(self):
		nationlist = worldstove.nukeparser.load_all('Nations')
		worldstove.nukeparser.find_nation("ea_ctis", nationlist)
		worldstove.nukeparser.find_nation("ea_c'tis", nationlist)
		worldstove.nukeparser.find_nation("MA Machaka", nationlist)
		worldstove.nukeparser.find_nation("LA R'lyeh", nationlist)
		worldstove.nukeparser.find_nation("EA NIEFELHEIM", nationlist)
		worldstove.nukeparser.find_nation("m   A   e   R   m   O   r", nationlist)

	def test_find_invalid_nation(self):
		nationlist = worldstove.nukeparser.load_all('Nations')
		with self.assertRaises(KeyError):
			worldstove.nukeparser.find_nation("ea nation", nationlist)
		with self.assertRaises(KeyError):
			worldstove.nukeparser.find_nation("qwertyuiop", nationlist)

if __name__ == '__main__':
	unittest.main()
