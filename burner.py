#!/usr/bin/env python

import os
import numpy as np
import argparse
import configparser
import worldstove


import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('players', nargs='+', help='Player nations. Specify as ea_arco, ma_ind, la_ulm or a generic start like islandstart, cavestart, or foreststart')
parser.add_argument('--thrones', type=int, help='Number of throne sites to generate')
parser.add_argument('--playerprovinces', type=int, default=12, help='Number of provinces to generate for each player')
#mapsize = parser.add_mutually_exclusive_group()
#by_spacing = mapsize.add_argument_group(title='By spacing')
#by_spacing.add_argument('--hspacing', type=int, default=180, help='Horizontal spacing of province grid')
#by_spacing.add_argument('--vspacing', type=int, default=120, help='Vertical spacing of province grid')
parser.add_argument('--hspacing', type=int, default=180, help='Horizontal spacing of province grid')
parser.add_argument('--vspacing', type=int, default=120, help='Vertical spacing of province grid')
parser.add_argument('--aspect', type=int, default=1.618, help='Aspect ratio of map (NOT PROVINCE GRID!)')
#
#by_shape = mapsize.add_argument_group(title='By image size')
#by_shape.add_argument('--imagesize', type=int, nargs=2, default=[1920, 1080], help='Width and height of image')

if os.environ.get('NATIONSPATH'): parser.add_argument('--datadir', default=os.environ['NATIONSPATH'], help='Location of Mapnuke nation definitions (default: $NATIONSPATH == {}'.format(os.environ['NATIONSPATH']))
else: parser.add_argument('--datadir', default='.', help='Location of Mapnuke nation definitions (default: (current directory))')
parser.add_argument('--configfile', default='default.cfg', help='Location of terrain config file (default: default.cfg)')


def compute_gridshape(args):
	#TODO: incorporate this into worldoven.stove.Grid so we can put thrones in 
	#the 3x3 box around start sites iff there is no direct connection

	thrones = len(args.players) if args.thrones is None else args.thrones
	minpoints = max(8 * len(args.players) + thrones, len(args.players) * args.playerprovinces)

	height = np.sqrt(minpoints * args.hspacing * args.vspacing / args.aspect)
	width = height * args.aspect

	height, width = np.int64(np.ceil(height)), np.int64(np.ceil(width))

	gridshape = np.int64(np.ceil(height / args.vspacing)), np.int64(np.ceil(width / args.hspacing))
	mapshape = gridshape[1] * args.vspacing, gridshape[0] * args.hspacing

	return gridshape, mapshape

#test functions
def plot_grid(args, gridshape):
	x, y = np.meshgrid(np.arange(gridshape[1]), np.arange(gridshape[0]))
	x = np.ravel(x) * args.hspacing + args.hspacing/2
	y = np.ravel(y) * args.vspacing + args.vspacing/2
	plt.plot(x, y, lw=0, marker='.')
def plot_starts(args, starts):
	plt.plot([p[1]*args.hspacing + args.hspacing/2 for p in starts], [p[0]*args.vspacing + args.vspacing/2 for p in starts], lw=0, marker='x', markersize=25)
	

def main(args):
	gridshape, mapshape = compute_gridshape(args)
	grid = worldstove.stove.Grid(gridshape, mapshape)
	nationlist = worldstove.nukeparser.load_all(args.datadir)
	nations = [worldstove.nukeparser.find_nation(name, nationlist) for name in args.players]

	cfgparser = configparser.ConfigParser()
	with open(args.configfile) as fh:
		config = cfgparser.read_file(fh)

	#starts, thrones = place_points(args, gridshape)
	thrones = len(args.players) if args.thrones is None else args.thrones
	grid.add_starts_thrones(len(args.players), thrones)
	grid.add_random_connections()
	grid.apply_starts(nations)
	grid.generate_terrain(nations, cfgparser)
	grid.assign_connections(cfgparser)
	grid.generate_voronoi()
	#worldstove.rendering.simple.render(grid)
	for i in range(2):
		grid.relax_points(steps=1)
		grid.relax_points_simple()
	worldstove.rendering.simple.render(grid)
	#grid.relax_points()
	#grid.relax_points_simple()
	#worldstove.rendering.simple.render(grid)

	print(grid.pretty_print(color=True))

	worldstove.rendering.simple.plt.show()

	#plt.show()

	gamemap = None
	return gamemap

if __name__ == '__main__':
	args = parser.parse_args()
	main(args)
