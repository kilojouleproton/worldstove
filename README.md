# Worldstove

A minimalistic map generator for Dominions 5 inspired by [Mapnuke](https://nuke-makes-games.itch.io/mapnuke) and [Cartographic Revision](https://corbeau.itch.io/cartographic-revision) intended for use in headless environments.

## Installation

Aside from Python 3, Worldstove depends heavily on Numpy and Matplotlib and requires a copy of the Mapnuke nation specification files.
Until I can get around to linking it properly (or mirroring Worldstove on Github), the easiest way is to download the [nation pack](https://github.com/nuke-makes-games/mapnuke/tree/master/Assets/Nations) somewhere and pointing to it by setting the environment variable `$NATIONSPATH` or just running burner.py with `--datadir /directory/containing/Nations.xml` .

## Usage

Generate a map for EA Arcoscephale, MA Ind, LA Ulm, and a generic cave start:

`python burner.py ea_arco ma_ind la_ulm cave`

Worldstove simplifies nation names somewhat, and the following variations on the above command line are equally acceptable:

 - `python burner.py EAARCOSCEPHALE ma__ind LaUlM C_A_V_E`

 - `python burner.py ea____arcos maIND LAulm cave`

## TODO

 - Incorporate Worldstove into a Discord.py bot

 - Procedurally generate all sprites

 - Replace the ridiculously messy and slow code for the Province and Grid class
